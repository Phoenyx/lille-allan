__author__ = 'render'
import bpy
from utils.script_utils import prompt

class SceneSettings(bpy.types.Operator):
    '''Sets the units to metres and the render to Cycles'''
    bl_idname = "la.scene_settings"
    bl_label = "Scene Settings Set Up"

    def execute(self, context):
        scene = bpy.context.scene
        scene.unit_settings.system = 'METRIC'
        scene.unit_settings.scale_length = 1
        # bpy.context.space_data.clip_start = 0.001
        scene.render.engine = 'CYCLES'
        bpy.context.scene.use_audio_scrub = True
        bpy.context.scene.use_audio_sync = True
        bpy.context.scene.render.resolution_x = 1920
        bpy.context.scene.render.resolution_y = 817
        bpy.ops.script.python_file_run(filepath="C:\\Program Files\\Blender Foundation\\Blender\\2.74\\scripts\\presets\\framerate\\25.py")
        prompt("Setup Complete")  # FEEDBACK TO USER
        return {'FINISHED'}

