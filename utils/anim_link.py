__author__ = 'render'
import re
import os
import shutil

import bpy
from scene.inc_save import IncrementFileVersion
from utils.script_utils import prompt
from utils.script_utils import ANIM_FILE_NAME_REGEX_NAME
from utils.script_utils import ANIM_FILE_NAME_REGEX_VERSION
from utils.script_utils import PROP_FILE_NAME_REGEX_VERSION
from utils.script_utils import PROP_FILE_NAME_REGEX_NAME


class MakeAnimLink(bpy.types.Operator):
    '''make anim link'''
    bl_idname = "make.alink"
    bl_label = "make anim link"

    PROP_NAME = PROP_FILE_NAME_REGEX_NAME
    PROP_VERSION = PROP_FILE_NAME_REGEX_VERSION
    ANIM_NAME = ANIM_FILE_NAME_REGEX_NAME
    ANIM_VERSION = ANIM_FILE_NAME_REGEX_VERSION

    def execute(self, context):
        curr_file_path = bpy.data.filepath
        main_folder = os.path.dirname(os.path.dirname(curr_file_path))
        scene_file_name = IncrementFileVersion.get_current_filename(self)

        if re.match(self.PROP_NAME, scene_file_name):
            matches = re.search(self.PROP_NAME, scene_file_name)
            groups = matches.groups()
            filename = groups[1]
            print (filename)

            animLink_folder = str(main_folder + "\\animLink")
            animLink_path = animLink_folder + "/" + filename + "_LINK" + ".blend"

            print (animLink_path)

            shutil.copy(curr_file_path, animLink_path)
            prompt("Made anim link file!")

        else:
            prompt("Cannot make an animLink - check scene naming.")

        return {'FINISHED'}
