__author__ = 'render'
import bpy

class ClearScale(bpy.types.Operator):
    '''Keeps current object size but sets the scale to 1 1 1'''
    bl_idname = "clear.scale"
    bl_label = "Freeze Object Scale"

    def execute(self, context):
        bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)
        return {'FINISHED'}