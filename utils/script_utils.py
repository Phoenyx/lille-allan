import os
import bpy

__author__ = 'render'

#
# Utilities used by the scripts and modules in Lille Allan Package
#

ADDON_NAME = "LilleAllan"
ADDON_DIR = os.path.dirname(os.path.dirname(__file__))

PROP_FILE_NAME_REGEX_VERSION = r'(([a-zA-z]+[0-9]*_v)([0-9]{3})_([a-zA-z]{2}))'

PROP_FILE_NAME_REGEX_NAME = r'(([a-zA-z]+[0-9]*)(_v[0-9]{3})_([a-zA-z]{2}))'

ANIM_FILE_NAME_REGEX_NAME = r'(SQ[0-9]{4}_SC[0-9]{4}_SH[0-9]{4})(_v[0-9]{3})_([a-zA-z]{2})'

ANIM_FILE_NAME_REGEX_VERSION = r'(SQ[0-9]{4}_SC[0-9]{4}_SH[0-9]{4})(_v)([0-9]{3})_([a-zA-z]{2})'

MDD_FILE_NAME_REGEX = r'([a-zA-z]+[0-9]*)(.*)'

keyframes = []  # Used by animation.key_extractor

is_prop_scene = False    # Used by inc_save


animation_folder = r"D:\\ProjectName\\02_pro\\animation\\"
asset_folder = r"D:\\ProjectName\\02_pro\\asset\\"
render_folder = r"D:\\ProjectName\\02_pro\\render\\"

def prompt(msg, icon='INFO'):
    def draw(popup, context):
        popup.layout.label("")

    bpy.context.window_manager.popup_menu(draw, title=msg, icon=icon)