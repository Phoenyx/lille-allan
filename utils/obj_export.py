__author__ = 'render'
import os
import re

import bpy
from utils.script_utils import prompt
from utils.script_utils import ANIM_FILE_NAME_REGEX_NAME


class ExportAllObj(bpy.types.Operator):
    '''Exports all objects parented under a group named "*_Mesh" as obj to export folder'''
    bl_idname = "outall.obj"
    bl_label = "Exported Selected As Obj's"

    ANIM_NAME = ANIM_FILE_NAME_REGEX_NAME

    def execute(self, context):
        scene_file_name_ext = os.path.splitext(bpy.path.basename(bpy.context.blend_data.filepath))
        filename = str(scene_file_name_ext[0])
        if re.match(self.ANIM_NAME, filename):
            curr_file_path = bpy.data.filepath
            main_folder = os.path.dirname(curr_file_path)
            export_folder = main_folder + "\export\\"

            if not os.path.exists(export_folder):
                print ("Creating {0}".format(export_folder))
                os.makedirs(export_folder)

            if bpy.data.objects.get("LittleAllan_Mesh") is not None:
                if bpy.data.objects["LittleAllan_Mesh"].hide:
                    if bpy.data.objects.get("stride_bone") is not None:     # Only present if anim has been retargeted
                        bpy.data.objects["stride_bone"].hide = False
                    bpy.data.objects["LittleAllan_Mesh"].hide = False      # Unhide!
                    print("SHOWING")

            parent_list = []
            bpy.ops.object.select_all(action='DESELECT')
            bpy.ops.object.select_pattern(pattern="*_Mesh")
            for obj in bpy.context.selected_objects:
                parent_list.append(obj)

            for folder in parent_list:  # Creates all parent folders in the export folder
                folder_name = str(folder.name[:-5])  # Removes "_Mesh" from the name
                filepath = export_folder + folder_name
                if not os.path.exists(filepath):
                    os.makedirs(filepath)

            for folder in parent_list:
                bpy.ops.object.select_all(action='DESELECT')
                bpy.ops.object.select_pattern(pattern=folder.name)
                bpy.ops.object.select_hierarchy(direction='CHILD', extend=False)
                bpy.ops.object.select_hierarchy(direction='CHILD', extend=True)
                show_sub_surf = False
                selection_number = len(bpy.context.selected_objects)
                export_number = 0
                for child in bpy.context.selected_objects:
                    if "Collision_Group" in child.name:
                        selection_number -= 1
                        continue
                    bpy.context.scene.objects.active = child
                    if 'SUBSURF' in (mod.type for mod in child.modifiers):
                        if bpy.context.object.modifiers["Subsurf"].show_viewport:
                            show_sub_surf = True
                            bpy.context.object.modifiers["Subsurf"].show_viewport = False

                    bpy.ops.object.select_all(action='DESELECT')
                    bpy.ops.object.select_pattern(pattern=child.name)
                    bpy.context.scene.frame_current = 1
                    if child.name == "collar_cloth":
                        parent_folder = "LittleAllan"
                        export_path = export_folder + str(parent_folder) + "\\" + "collar_cloth.obj"
                        bpy.ops.export_scene.obj(filepath=export_path, axis_forward='-Z', axis_up='Y', use_selection=True)
                        export_number += 1
                    else:
                        parent_folder = child.parent.name[:-5]
                        export_path = export_folder + str(parent_folder) + "\\" + str(child.name) + ".obj"

                        bpy.ops.export_scene.obj(filepath=export_path, axis_forward='-Z', axis_up='Y', use_selection=True)
                        export_number += 1

                    if show_sub_surf:
                        bpy.context.object.modifiers["Subsurf"].show_viewport = True
            prompt("Exported " + str(export_number) + " of " + str(selection_number) + " objects")
        else:
            prompt("Failed to export - check scene naming.", icon="ERROR")

        if bpy.data.objects.get("stride_bone") is not None:     # Only present if anim has been retargeted
            bpy.data.objects["stride_bone"].hide = True
        if bpy.data.objects.get("LittleAllan_Mesh") is not None:
            bpy.data.objects["LittleAllan_Mesh"].hide = True
        return {'FINISHED'}


class ExportSelectedObj(bpy.types.Operator):
    '''Exports selected objects to export folder'''
    bl_idname = "out.obj"
    bl_label = "Exported Selected As Objs"

    ANIM_NAME = ANIM_FILE_NAME_REGEX_NAME

    def execute(self, context):
        scene_file_name_ext = os.path.splitext(bpy.path.basename(bpy.context.blend_data.filepath))
        filename = str(scene_file_name_ext[0])
        if re.match(self.ANIM_NAME, filename):      # See if we are in an animation scene
            curr_file_path = bpy.data.filepath
            main_folder = os.path.dirname(curr_file_path)
            export_folder = main_folder + "\export\\"

            # Get file path to the export path and create the export folder if it doesnt exist
            if not os.path.exists(export_folder):
                print ("Creating {0}".format(export_folder))
                os.makedirs(export_folder)

            selection_number = len(bpy.context.selected_objects)
            export_number = 0
            for obj_to_export in bpy.context.selected_objects:
                show_sub_surf = False       # to know if we need to show the modifier in the viewport later
                bpy.context.scene.objects.active = obj_to_export
                if 'SUBSURF' in (mod.type for mod in obj_to_export.modifiers):
                    if bpy.context.object.modifiers["Subsurf"].show_viewport:
                        show_sub_surf = True
                        bpy.context.object.modifiers["Subsurf"].show_viewport = False

                bpy.context.scene.frame_current = 1     # If the obj doesnt move we want its' pose on frame 1

                if obj_to_export.name == "collar_cloth":    # Special case - hopefully will be fixed for production
                    parent_folder = "LittleAllan"
                    export_path = export_folder + str(parent_folder) + "\\" + "collar_cloth.obj"
                    bpy.ops.export_scene.obj(filepath=export_path, axis_forward='-Z', axis_up='Y', use_selection=True)
                    export_number += 1
                else:
                    parent_folder = obj_to_export.parent.name[:-5]      # remove "Mesh" from name
                    export_path = export_folder + str(parent_folder) + "\\" + str(obj_to_export.name) + ".obj"
                    filepath = export_folder + str(parent_folder)   # Create parent folder if it doesnt exist
                    if not os.path.exists(filepath):
                        os.makedirs(filepath)
                    bpy.ops.export_scene.obj(filepath=export_path, axis_forward='-Z', axis_up='Y', use_selection=True)
                    export_number += 1

                if show_sub_surf:
                    bpy.context.object.modifiers["Subsurf"].show_viewport = True    # Turn on the modifier
            prompt("Exported " + str(export_number) + " objects of " + str(selection_number) + " selected")
        else:
            prompt("Failed to export - check scene naming.", icon="ERROR")
        return {'FINISHED'}
