__author__ = 'Sofia'
import configparser as ConfigParser
import os

__author__ = 'render'

config = None

def get_setting(key, LIGHT_SECTION, LIGHTS_TXT_FILE_PATH):
    global config                                       # "key" being the setting we want the value for

    new_section = LIGHT_SECTION                         # = "Light1", "Light2", etc
    lights_txt_file_path = LIGHTS_TXT_FILE_PATH

    _load_config(new_section, lights_txt_file_path)

    if config is not None:
        if key in config:
            return config[key]                          # Return the value of the key given
    return None

def _load_config(new_section, lights_txt_file_path):
    global config

    if config is None:
        if not os.path.exists(lights_txt_file_path):        # Check if txt file exists
            print ("No .txt file")

    cfg_parser = ConfigParser.ConfigParser()

    def map_config(section):
        dict1 = {}
        settings = cfg_parser.options(section)
        for setting in settings:
            dict1[setting] = cfg_parser.get(section, setting)       # Get the value of the setting
        return dict1                                                # Returns a dictionary with setting as value

    try:
        cfg_parser.read(lights_txt_file_path)
        config = map_config(new_section)                            # Set config to be the dictionary

    except Exception as e:
        print("Failed reading txt file", e)
