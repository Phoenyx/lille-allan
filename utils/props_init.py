__author__ = 'render'
import bpy


class InitMyPropOperator(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "scene.init_my_prop"
    bl_label = "Init my_prop"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        if context.scene.AssetName != "":
            self.__class__.bl_label = "Change AssetName"
        else:
            context.scene.AssetName = "foobar"
            self.__class__.bl_label = self.bl_label

        if context.scene.MyEnum != "":
            context.scene.MyEnum = " "
            self.__class__.bl_label = "Change myEnum"
        else:
            context.scene.MyEnum = "foobar"
            self.__class__.bl_label = self.bl_label

        if context.scene.CharEnum != "":
            context.scene.CharEnum = " "
            self.__class__.bl_label = "Change CharEnum"
        else:
            context.scene.CharEnum = "foobar"
            self.__class__.bl_label = self.bl_label

        return {'FINISHED'}