__author__ = 'render'
import bpy
import os


class ImportReferenceChair(bpy.types.Operator):
    '''Imports a 8cm high chair for reference'''
    bl_idname = "my.chair"
    bl_label = "Import 45cm Reference Chair"

    def execute(self, context):
        this_filepath = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + os.sep + "referenceChair_8cm.obj"
        bpy.ops.import_scene.obj(filepath=this_filepath, axis_forward='-Y', axis_up='-Z')
        return {'FINISHED'}


class ImportReferenceCube(bpy.types.Operator):
    '''Imports a 1 meter by 1 meter cube for reference'''
    bl_idname = "my.cube"
    bl_label = "Import 1x1m Reference Cube"

    def execute(self, context):
        this_filepath = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + os.sep + "referenceCube.obj"
        bpy.ops.import_scene.obj(filepath=this_filepath, axis_forward='-Y', axis_up='-Z')
        return {'FINISHED'}