__author__ = 'render'
import os
import re

import bpy
from bpy.props import StringProperty, IntProperty, BoolProperty
from bpy_extras.io_utils import ExportHelper
from utils.script_utils import prompt
from utils.script_utils import ANIM_FILE_NAME_REGEX_NAME


class ExportAllMdd(bpy.types.Operator, ExportHelper):
    '''Exports all objects parented under a group named "*_Mesh" to MDD vertex keyframe file'''
    bl_idname = "outall.mdd"
    bl_label = "Export all mesh to MDD"
    i = -1
    filename_ext = ".mdd"
    filter_glob = StringProperty(default="*.mdd", options={'HIDDEN'})

    ANIM_NAME = ANIM_FILE_NAME_REGEX_NAME


    # get first scene to get min and max properties for frames, fps
    minframe = 1
    maxframe = 600
    minfps = 1
    maxfps = 120

    # List of operator properties, the attributes will be assigned
    # to the class instance from the operator settings before calling.
    fps = IntProperty(
        name="Frames Per Second",
        description="Number of frames/second",
        min=minfps, max=maxfps,
        default=25,
    )
    frame_start = IntProperty(
        name="Start Frame",
        description="Start frame for baking",
        min=minframe, max=maxframe,
        default=1,
    )
    frame_end = IntProperty(
        name="End Frame",
        description="End frame for baking",
        min=minframe, max=maxframe,
        default=250,
    )
    flip_yz = BoolProperty(
        name="Export Flipped Y and Z axis",
        description="Export Flipped Y and Z axis",
        default=True,
    )

    @classmethod
    def poll(cls, context):
        obj = context.active_object
        return (obj and obj.type == 'MESH')

    def getexportpath(self, context, folder):
        curr_file_path = bpy.data.filepath
        main_folder = os.path.dirname(curr_file_path)
        export_folder = (main_folder + "\export\\")

        selected = str(folder)[:-5]  # Removes "_Mesh" from the name
        export_path = export_folder + "\\" + selected

        return export_path

    def invoke(self, context, event):
        scene_file_name_ext = os.path.splitext(bpy.path.basename(bpy.context.blend_data.filepath))
        filename = str(scene_file_name_ext[0])
        if re.match(self.ANIM_NAME, filename):
            parent_list = []
            bpy.ops.object.select_all(action='DESELECT')
            if bpy.ops.object.select_pattern(pattern="*_Mesh"):
                bpy.ops.object.select_pattern(pattern="*_Mesh")
                for obj in bpy.context.selected_objects:
                    parent_list.append(obj.name)

                for folder in parent_list:  # Creates all parent folders in the export folder
                    filepath = self.getexportpath(context, folder)
                    if not os.path.exists(filepath):
                        os.makedirs(filepath)

                if bpy.data.objects.get("LittleAllan_Mesh") is not None:
                    if bpy.data.objects["LittleAllan_Mesh"].hide:
                        if bpy.data.objects.get("stride_bone") is not None:     # Only present if anim has been retargeted
                            bpy.data.objects["stride_bone"].hide = False
                        bpy.data.objects["LittleAllan_Mesh"].hide = False      # Unhide! Cannot select if hidden
                        print("SHOWING")

                bpy.ops.object.select_pattern(pattern="*_Mesh")
                bpy.ops.object.select_hierarchy(direction='CHILD', extend=False)
                bpy.ops.object.select_hierarchy(direction='CHILD', extend=True)     # Select sub-children (collar_cloth)
                selection_number = len(bpy.context.selected_objects)
                export_number = 0
                for child in bpy.context.selected_objects:
                    if "Collision_Group" in child.name:
                        selection_number -= 1
                        continue
                    bpy.ops.object.select_all(action='DESELECT')
                    bpy.ops.object.select_pattern(pattern=child.name)
                    bpy.context.scene.objects.active = child

                    """
                    try:        # TODO: CODE BELOW DOESNT WORK ON ANIMATED RIGGED CHARS:
                        fcurves = len(child.animation_data.action.fcurves)
                        for i in range(fcurves):
                            points = points + len(child.animation_data.action.fcurves[i].keyframe_points)
                    except(AttributeError):
                        fcurves = 1
                        points = 1

                    try:
                        dfcurves = len(child.data.shape_keys.animation_data.action.fcurves)
                        for i in range(dfcurves):
                            dpoints = dpoints + len(child.data.shape_keys.animation_data.action.fcurves[i].keyframes_points)
                    except(AttributeError):
                        dpoints = 1
                        dfcurves = 1

                    if (points / fcurves) > 1 or (dpoints/dfcurves) > 1:"""

                    if child.name == "collar_cloth":
                        parent_name = "LittleAllan_Mesh"
                        self.filepath = self.getexportpath(context, parent_name) + "\\" + "collar_cloth.mdd"
                        self.execute(context)
                        export_number += 1
                    else:
                        self.filepath = self.getexportpath(context, str(child.parent.name)) + "\\" + child.name + ".mdd"
                        self.execute(context)
                        export_number += 1

                prompt("Exported " + str(export_number) + " of " + str(selection_number) + " objects")
            else:
                prompt("Found no _Mesh groups", icon="ERROR")
        else:
            prompt("Failed to export - check scene naming.", icon="ERROR")

        if bpy.data.objects.get("stride_bone") is not None:     # Only present if anim has been retargeted
            bpy.data.objects["stride_bone"].hide = True
        if bpy.data.objects.get("LittleAllan_Mesh") is not None:
            bpy.data.objects["LittleAllan_Mesh"].hide = True
        return {'FINISHED'}

    def execute(self, context):
        # initialize from scene if unset
        scene = context.scene
        self.frame_start = bpy.context.scene.frame_start
        self.frame_end = bpy.context.scene.frame_end
        self.fps = scene.render.fps

        start_frame = bpy.context.scene.frame_start
        end_frame = bpy.context.scene.frame_end+1

        # Need to find the real location or put it inside of the same folder as this script using
        # from . import export_mdd
        from io_shape_mdd import export_mdd

        return export_mdd.save(self, context, filepath=self.filepath, frame_start=start_frame, frame_end=end_frame), {'FINISHED'}


class ExportSelectedMdd(bpy.types.Operator, ExportHelper):
    '''Exports selected objects to MDD vertex keyframe file'''
    bl_idname = "out.mdd"
    bl_label = "Export selected mesh to MDD"
    i = -1
    filename_ext = ".mdd"
    filter_glob = StringProperty(default="*.mdd", options={'HIDDEN'})

    ANIM_NAME = ANIM_FILE_NAME_REGEX_NAME

    # get first scene to get min and max properties for frames, fps
    minframe = 1
    maxframe = 600
    minfps = 1
    maxfps = 120

    # List of operator properties, the attributes will be assigned
    # to the class instance from the operator settings before calling.
    fps = IntProperty(
        name="Frames Per Second",
        description="Number of frames/second",
        min=minfps, max=maxfps,
        default=25,
    )
    frame_start = IntProperty(
        name="Start Frame",
        description="Start frame for baking",
        min=minframe, max=maxframe,
        default=1,
    )
    frame_end = IntProperty(
        name="End Frame",
        description="End frame for baking",
        min=minframe, max=maxframe,
        default=250,
    )
    flip_yz = BoolProperty(
        name="Export Flipped Y and Z axis",
        description="Export Flipped Y and Z axis",
        default=True,
    )

    @classmethod
    def poll(cls, context):
        print ("polled")
        obj = context.active_object # Dont want to run the function unles we have mesh selected
        return (obj and obj.type == 'MESH')

    def getexportpath(self, context, folder):
        curr_file_path = bpy.data.filepath
        main_folder = os.path.dirname(curr_file_path)
        export_folder = (main_folder + "\export\\")

        selected = str(folder)[:-5]  # Removes "_Mesh" from the name
        export_path = export_folder + "\\" + selected
        print (export_path)
        return export_path

    def invoke(self, context, event):
        print ("invoked")
        scene_file_name_ext = os.path.splitext(bpy.path.basename(bpy.context.blend_data.filepath))
        filename = str(scene_file_name_ext[0])
        if re.match(self.ANIM_NAME, filename):      # Check that we are in an animation scene

            selection_number = len(bpy.context.selected_objects)
            export_number = 0
            for object in bpy.context.selected_objects:
                filepath = self.getexportpath(context, object.parent.name)
                if not os.path.exists(filepath):
                    os.makedirs(filepath)

                bpy.ops.object.select_all(action='DESELECT')
                object.select = True
                bpy.context.scene.objects.active = object

                if object.name == "collar_cloth":
                    parent_name = "LittleAllan_Mesh"
                    self.filepath = self.getexportpath(context, parent_name) + "\\" + "collar_cloth.mdd"
                    self.execute(context)
                    export_number += 1
                else:
                    self.filepath = self.getexportpath(context, str(object.parent.name)) + "\\" + object.name + ".mdd"
                    self.execute(context)
                    export_number += 1
                prompt("Exported " + str(export_number) + " objects of " + str(selection_number) + " selected")
        else:
            prompt("Failed to export - check scene naming.", icon="ERROR")
        return {'FINISHED'}

    def execute(self, context):
        # initialize from scene if unset
        scene = context.scene
        self.frame_start = bpy.context.scene.frame_start
        self.frame_end = bpy.context.scene.frame_end
        self.fps = scene.render.fps

        start_frame = bpy.context.scene.frame_start
        end_frame = bpy.context.scene.frame_end+1

        # Need to find the real location or put it inside of the same folder as this script using
        # from . import export_mdd
        from io_shape_mdd import export_mdd
        return export_mdd.save(self, context, filepath=self.filepath, frame_start=start_frame, frame_end=end_frame), {'FINISHED'}