import configparser as ConfigParser
import os
from utils.script_utils import ADDON_DIR

__author__ = 'render'

CONFIG_NAME = "config.ini"
CONFIG_SECTION = "CONFIG"

DEFAULTS = {
    "update_dir": r"\\READYNASPRO\media\projects\Lille Allan\TeaserProduction\05_pipeline\scripts\blender scripts\LilleAllan"
}

config_file = os.path.join(ADDON_DIR, CONFIG_NAME)
config = None


def get_setting(key):
    global config
    __load_cfg()

    if config is not None:
        if key in config:
            return config[key]
        elif key in DEFAULTS:
            DEFAULTS[key]

    return None


def set_setting(key, value):
    # TODO: Persist this onto disk
    global config
    __load_cfg()

    if config is not None:
        config[key] = value


def __load_cfg():
    #TODO: Collapse this into a single load_config function
    global config

    if config is None:
        if not os.path.exists(config_file):
            _create_config()

        _load_config()


def _create_config():
    cfg_parser = ConfigParser.ConfigParser()
    cfg_parser.add_section(CONFIG_SECTION)

    for key, val in DEFAULTS.items():
        cfg_parser.set(CONFIG_SECTION, key, val)

    cfg = open(config_file, "w")
    cfg_parser.write(cfg)


def _load_config():
    global config
    cfg_parser = ConfigParser.ConfigParser()

    def map_config(section):
        dict1 = {}
        options = cfg_parser.options(section)
        for option in options:
            dict1[option] = cfg_parser.get(section, option)
        return dict1

    try:
        cfg_parser.read(config_file)
        config = map_config(CONFIG_SECTION)
    except Exception as e:
        print("Failed reading config", e)