import pkgutil
import sys
import bpy
import os
import shutil
import imp
from utils.config import get_setting
from utils.script_utils import prompt, ADDON_NAME, ADDON_DIR


class UpdateScripts(bpy.types.Operator):
    """ Updates all the scripts in the LA script package - copies them from the network.
    """
    bl_idname = "update.scripts"
    bl_label = "Updates the entire script package"
    IGNORED_FILES = ["config.ini", "__pycache__"]

    update_dir = get_setting("update_dir")

    @staticmethod
    def copy_scripts(source, dest):
        def copy(sources, is_file):
            if not os.path.exists(dest):
                os.makedirs(dest)

            for src in sources:
                # Don't copy git/pycharm files
                if src.startswith(".") or src in UpdateScripts.IGNORED_FILES:
                    print("Skipping {0}".format(src))
                    continue

                abs_src = os.path.join(source, src)
                abs_dest = os.path.join(dest, src)

                # If its a dir
                if not is_file and os.path.exists(abs_dest):
                    shutil.rmtree(abs_dest)

                if is_file:
                    shutil.copyfile(abs_src, abs_dest)
                else:
                    shutil.copytree(abs_src, abs_dest)

        if os.path.exists(source):
            for root, dirs, files in os.walk(source):
                copy(files, is_file=True)
                copy(dirs, is_file=False)
                return True
        else:
            prompt("Failed to find the source path {0}".format(source), "ERROR")
            return False

    @staticmethod
    def reload_scripts():
        def reload_submodules(parent, mod):
            success = True
            for importer, modname, is_pkg in pkgutil.iter_modules(mod.__path__):
                if is_pkg:
                    if not reload_submodules(modname, sys.modules[modname]):
                        success = False
                else:
                    try:
                        imp.reload(sys.modules[parent + "." + modname])
                    except KeyError:
                        success = False
                        prompt("Error loading {0}, the module is not found in sys.modules.".format((parent + "." + modname)), "ERROR")
            return success

        ret = reload_submodules(ADDON_NAME, sys.modules[ADDON_NAME])

        bpy.ops.wm.addon_disable(module=ADDON_NAME)
        bpy.ops.wm.addon_enable(module=ADDON_NAME)
        return ret

    def execute(self, context):
        ret = UpdateScripts.copy_scripts(self.update_dir, ADDON_DIR)
        
        if ret is True:
            ret = UpdateScripts.reload_scripts()

        if ret is True:
            prompt("Update Completed!!")
            
        return {'FINISHED'}