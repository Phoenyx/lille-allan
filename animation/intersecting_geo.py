__author__ = 'render'

import bpy
import bmesh
from utils.script_utils import prompt


def bmesh_copy_from_object(obj, transform=True, triangulate=True, apply_modifiers=True):
    """
    Returns a transformed, triangulated copy of the mesh
    """

    assert (obj.type == 'MESH')

    if apply_modifiers and obj.modifiers:
        me = obj.to_mesh(bpy.context.scene, True, 'PREVIEW', calc_tessface=False)
        bm = bmesh.new()
        bm.from_mesh(me)
        bpy.data.meshes.remove(me)
    else:
        me = obj.data
        if obj.mode == 'EDIT':
            bm_orig = bmesh.from_edit_mesh(me)
            bm = bm_orig.copy()
        else:
            bm = bmesh.new()
            bm.from_mesh(me)

    # Remove custom data layers to save memory
    for elem in (bm.faces, bm.edges, bm.verts, bm.loops):
        for layers_name in dir(elem.layers):
            if not layers_name.startswith("_"):
                layers = getattr(elem.layers, layers_name)
                for layer_name, layer in layers.items():
                    layers.remove(layer)

    if transform:
        bm.transform(obj.matrix_world)

    if triangulate:
        bmesh.ops.triangulate(bm, faces=bm.faces)

    return bm

def bmesh_check_intersect_objects(obj, obj2):
    """
    Check if any faces intersect with the other object

    returns a boolean
    """
    assert (obj != obj2)

    # Triangulate
    bm = bmesh_copy_from_object(obj, transform=True, triangulate=True)
    bm2 = bmesh_copy_from_object(obj2, transform=True, triangulate=True)

    # If bm has more edges, use bm2 instead for looping over its edges
    # (so we cast less rays from the simpler object to the more complex object)
    if len(bm.edges) > len(bm2.edges):
        bm2, bm = bm, bm2

    # Create a real mesh (lame!)
    scene = bpy.context.scene
    me_tmp = bpy.data.meshes.new(name="~temp~")
    bm2.to_mesh(me_tmp)
    bm2.free()
    obj_tmp = bpy.data.objects.new(name=me_tmp.name, object_data=me_tmp)
    scene.objects.link(obj_tmp)
    scene.update()
    ray_cast = obj_tmp.ray_cast

    intersect = False

    EPS_NORMAL = 0.000001
    EPS_CENTER = 0.01  # should always be bigger

    # for ed in me_tmp.edges:
    for ed in bm.edges:
        v1, v2 = ed.verts

        # setup the edge with an offset
        co_1 = v1.co.copy()
        co_2 = v2.co.copy()
        co_mid = (co_1 + co_2) * 0.5
        no_mid = (v1.normal + v2.normal).normalized() * EPS_NORMAL
        co_1 = co_1.lerp(co_mid, EPS_CENTER) + no_mid
        co_2 = co_2.lerp(co_mid, EPS_CENTER) + no_mid

        co, no, index = ray_cast(co_1, co_2)
        if index != -1:
            intersect = True
            break

    scene.objects.unlink(obj_tmp)
    bpy.data.objects.remove(obj_tmp)
    bpy.data.meshes.remove(me_tmp)

    scene.update()

    return intersect


class IntersectCheck(bpy.types.Operator):
    '''Checks for mesh intersections in the scene'''
    bl_idname = "int.check"
    bl_label = "Checks for mesh intersections in the scene"

    def execute(self, context):
        for area in bpy.context.screen.areas:
            if area.type == 'VIEW_3D':
                area.spaces.active.region_3d.view_matrix
                break
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')

        print("_____________    New intersection test    _____________ ")
        collision_list = []
        frame_numbers = []
        count = 0
        last_intersected = False

        start_frame = bpy.context.scene.frame_start
        end_frame = bpy.context.scene.frame_end

        bpy.context.scene.frame_set(start_frame)
        print ("")
        print ("Frame test range: " + str(start_frame) + " to " + str(end_frame))

        # set the active area in Blender to be the 3d view
        for area in bpy.context.screen.areas:
            if area.type == 'VIEW_3D':
                area.spaces.active.region_3d.view_matrix
                break

        bpy.ops.object.select_all(action='DESELECT')
        # Select all objects named Collision_Group
        bpy.ops.object.select_pattern(pattern="*Collision_Group")

        intersections_count = 0     # Used to count how many intersections happened during testing

        for selected in bpy.context.selected_objects:
            collision_list = []

            for child in selected.children:
                bpy.data.objects[child.name].hide_select = False    # make selectable
                bpy.data.objects[child.name].hide = False           # Unhide
                bpy.context.scene.objects.active = child
                bpy.ops.object.mode_set(mode='OBJECT')
                collision_list.append(child)                        # add child to the list of objects were checking for collisions

            bpy.ops.object.select_all(action='DESELECT')
            selected.select = True                                  # select the current Collision_Group
            bpy.context.scene.frame_set(start_frame)

            print ("")
            print ("   ---- Processing next character ----     ")
            print ("")
            print ("      Current test subject: " + str(selected.name[:-16]))
            print ("")

            for frame in range(start_frame, end_frame + 1, 1):      # Loop over all the frames
                bpy.context.scene.frame_set(frame)
                count = 0                                           # count being an index in the collision_list
                last_intersected = False

                for k in range(len(collision_list)):                # k being an index in the list

                    for i in range(len(collision_list)):            # i being the number of elements in the list

                        try:
                            count += 1
                            obj1 = collision_list[k]                # set obj1 to be index k
                            obj2 = collision_list[count]            # set obj2 to be index count

                            # check if they are intersecting on the current frame:
                            intersect = bmesh_check_intersect_objects(obj1, obj2)

                            if intersect:       # if they intersected then give feedback
                                last_intersected = True
                                intersections_count += 1
                                print(collision_list[k].name, collision_list[count].name,
                                      "are intersecting on frame: " + str(frame))

                        except IndexError:
                            continue

                    count = k + 1       # Go to next element in list

                if last_intersected:
                    print("____________________________________________________________")
                    print ("")

            for limb in collision_list:
                bpy.data.objects[limb.name].hide_select = True      # make unselectable again
                bpy.data.objects[limb.name].hide = True             # hide again

        print("_____________    Intersection test done   _____________ ")

        if intersections_count != 0:
            prompt("Test done, found " + str(intersections_count) + " intersections, look in console.")
        else:
            prompt("Test done, found no intersections")
        return {'FINISHED'}
