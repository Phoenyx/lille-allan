__author__ = 'render'

import os
import re

import bpy
from utils.script_utils import prompt

from utils.script_utils import ANIM_FILE_NAME_REGEX_NAME


class CreateMainCam(bpy.types.Operator):
    """Creates a camera named mainCamera"""
    bl_idname = "cam.create"
    bl_label = "Create a main camera for the scene"

    def execute(self, context):
        bpy.ops.object.select_all(action='DESELECT')
        if len(bpy.context.selected_objects) < 1:
            if bpy.data.objects.get("mainCamera") is None:
                bpy.ops.object.camera_add(location=(0, 0, 0), rotation=(1.5708, 0, 0))  # 1.507 = 90 degrees
                for obj in bpy.context.selected_objects:
                    obj.name = "mainCamera"
                prompt("mainCamera created!")
            else:
                prompt("mainCamera already exists!")
        else:
            prompt("Please clear your selection!")
        return {'FINISHED'}


class CamExportFBX(bpy.types.Operator):
    """Exports the camera in scene named mainCamera"""
    bl_idname = "cam_out.fbx"
    bl_label = "Exported Camera As FBX"

    ANIM_NAME = ANIM_FILE_NAME_REGEX_NAME

    def execute(self, context):
        scene_file_name_ext = os.path.splitext(bpy.path.basename(bpy.context.blend_data.filepath))
        filename = str(scene_file_name_ext[0])
        curr_file_path = bpy.data.filepath
        main_folder = os.path.dirname(curr_file_path)
        export_folder = main_folder + "\export\\"

        if re.match(self.ANIM_NAME, filename):
            matches = re.search(self.ANIM_NAME, filename)
            groups = matches.groups()
            filename = groups[0]
        else:
            prompt("Failed to export - check scene naming.")

        if not os.path.exists(export_folder):
            print("Creating {0}".format(export_folder))
            os.makedirs(export_folder)

        export_path = export_folder + filename + "_mainCamera.fbx"

        if bpy.data.objects["mainCamera"].hide_select:
            bpy.data.objects["mainCamera"].hide_select = False

        bpy.ops.object.select_all(action='DESELECT')
        bpy.ops.object.select_pattern(pattern="mainCamera")
        if len(bpy.context.selected_objects) == 1:
            prompt("'mainCamera' exported to " + export_path)  # FEEDBACK TO USER
            bpy.ops.export_scene.fbx(filepath=export_path, filter_glob="*.fbx", axis_forward='Y', axis_up='Z',
                                     bake_anim_use_nla_strips=False, bake_anim_use_all_actions=False,
                                     use_selection=True)
        else:
            prompt("No mainCamera found")  # FEEDBACK TO USER

        return {'FINISHED'}

