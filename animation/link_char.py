__author__ = 'render'
import bpy
import bpy
from os.path import dirname, basename
from bpy.props import *
from utils.script_utils import prompt

class LinkRiggedChars(bpy.types.Operator):
    '''Get character for animation'''
    bl_idname = "link.chars"
    bl_label = "Link scene to import characters for animation"

    def execute(self, context):
        prompt("This feature is still being developed")
        return {'FINISHED'}


""" _______ WORK IN PROGRESS ________
filepath = "M:\\projects\\Lille Allan\\TeaserProduction\\02_production\\asset\\character\\LilleAllan\\animLink\\littleAllan_LINK.blend"

datablock_dir = "\\Object\\"
filename = "\\" + basename(filepath) + datablock_dir
directory = filepath + datablock_dir
parts = ("body" "collar_cloth" "eyes")
therest = ("LittleAllan_armature, LittleAllan_Mesh, LittleAllan_Rig, LittleAllan_widgets, shirt_cloth, shoes, shorts_cloth")


class UIPanel(bpy.types.Panel):
    bl_label = "Link Panel"
    bl_idname = "SCENE_PT_object_lib"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    #bl_context = "scene"

    update = True
    refreshed = False
    obs = []

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.operator(GetChar.bl_idname, "Get Objs")


class GetChar(bpy.types.Operator):
    bl_idname = "link.char"
    bl_label = "update"

    def execute(self, context):

        #   Below imports from hardcoded list
        #bpy.ops.wm.link(filepath=filename, directory=directory, filename=parts, link=True, instance_groups=False)

        #   Below imports from script-created list
        item_list(self, context)    # Make list of objects to import

        if bpy.ops.wm.link.poll():
            for obj in UIPanel.obs:
                name = str(obj[0])
                if not name.startswith("WGT"):      # Filter objects in list
                    bpy.ops.wm.link(filepath=filename, directory=directory, filename=name, link=True, instance_groups=False)
        return {'FINISHED'}


def item_list(self, context):
    if UIPanel.update:
        UIPanel.obs = []
        # print list of materials with users, if present.
        with bpy.data.libraries.load(filepath) as (data_from, data_to):
            if data_from.objects:
                for ob in data_from.objects:
                    UIPanel.obs.append((ob, ob, ''))
                    UIPanel.update = False

    return UIPanel.obs


def register():
    bpy.utils.register_class(UIPanel)
    bpy.utils.register_class(GetChar)


def unregister():
    bpy.utils.unregister_class(UIPanel)
    bpy.utils.unregister_class(GetChar)


if __name__ == "__main__":
    register()
"""