__author__ = 'render'
import os

import bpy
from utils.script_utils import prompt

class CreatePlayblast(bpy.types.Operator):
    '''Creates an animation video (viewport render)'''
    bl_idname = "playblast.output"
    bl_label = "Creates an animation video (viewport render)"

    def SetCameraView(self, context):
        for a in context.window.screen.areas:
            if a.type == 'VIEW_3D':  # Need to be in the 3D view to set the active object and camera
                bpy.ops.object.select_all(action='DESELECT')
                bpy.ops.object.select_pattern(pattern="mainCamera")
                if len(bpy.context.selected_objects) == 1:  # Checking that there's only 1 mainCamera
                    bpy.context.scene.objects.active = bpy.data.objects["mainCamera"]
                    bpy.ops.view3d.object_as_camera()
                    return True
                else:
                    prompt("More than one mainCamera!", "ERROR")
                    return {'FINISHED'}
            else:
                continue
        return {'FINISHED'}

    def DoPlayblast(self, context):
        bpy.ops.render.opengl(animation=True, view_context=True)
        return {'FINISHED'}

    def execute(self, context):

        for e in bpy.data.screens[bpy.context.screen.name].areas:
            if e.type == 'VIEW_3D':
                e.spaces.active.show_only_render = True
                e.spaces.active.viewport_shade = 'TEXTURED'

        bpy.data.objects["mainCamera"].hide_select = False
        bpy.ops.object.select_all(action='DESELECT')
        bpy.ops.object.select_pattern(pattern="mainCamera")

        if len(bpy.context.selected_objects) != 1:
            prompt("No mainCamera found")  # Checking that there is a mainCamera
            return {'FINISHED'}

        else:
            if self.SetCameraView(context):
                scene_file_name_ext = os.path.splitext(bpy.path.basename(bpy.context.blend_data.filepath))
                scene_file_name = str(scene_file_name_ext[0])
                scene = bpy.context.scene
                scene.render.image_settings.file_format = 'AVI_JPEG'
                # Filename example: Q0010_S0010_v007_rr_60frames
                scene.render.filepath = "//export/" + scene_file_name + "_" + str(scene.frame_end) + "frames.avi"

                try:
                    if self.DoPlayblast(context):
                        prompt("Playblasted to export folder")
                    for e in bpy.data.screens[bpy.context.screen.name].areas:
                        if e.type == 'VIEW_3D':
                            e.spaces.active.show_only_render = False
                except RuntimeError:
                    prompt("Please close all .avi files")
            return {'FINISHED'}