__author__ = 'render'
import bpy
from utils.script_utils import prompt
from utils.script_utils import keyframes

class AnimExtractor(bpy.types.Operator):
    '''Deletes all but the added keyframes in the list above on all controllers/objects.'''
    bl_idname = "anim.extractor"
    bl_label = "Extract key poses from the mo-cap animation"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    framenumbers = []

    def execute(self, context):
        wm = bpy.context.window_manager

        frame_end = bpy.context.scene.frame_end
        frame_start = bpy.context.scene.frame_start

        for i in range(frame_start, frame_end + 1, 1):
            self.framenumbers.append(i)

        wm.progress_begin(0, frame_end)         # Shows progress via numbers on the cursor

        for frame in self.framenumbers:

            if ((int(frame)/int(frame_end))*100) % 10 == 0:     # Update every 10 percent
                wm.progress_update(int(frame))

            if frame not in keyframes:
                bpy.context.scene.frame_set(frame=frame)
                bpy.ops.anim.keyframe_delete_v3d()
        wm.progress_end()
        prompt("Done!")
        return {'FINISHED'}


class AddKeyframes(bpy.types.Operator):
    '''Adds current keyframe to the list'''
    bl_idname = "add.keyframe"
    bl_category = "LA Tools"
    bl_label = "Scene"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"

    def execute(self, context):
        keyframe_to_add = bpy.context.scene.frame_current
        keyframes.append(keyframe_to_add)
        print(keyframes)
        for key in keyframes:
            if keyframes.count(key) > 1:
                keyframes.remove(key)
        keyframes.sort()
        return {'FINISHED'}


class RemoveKeyframes(bpy.types.Operator):
    '''Remove current keyframe from the list'''
    bl_idname = "remove.keyframe"
    bl_category = "LA Tools"
    bl_label = "Delete the current keyframe from the list"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"

    def execute(self, context):
        keyframe_to_add = bpy.context.scene.frame_current
        if keyframes.count(keyframe_to_add) == 1:
            keyframes.remove(keyframe_to_add)
        keyframes.sort()
        return {'FINISHED'}

class ClearKeyframes(bpy.types.Operator):
    '''Clears the list of keyframes'''
    bl_idname = "clear.keyframe"
    bl_category = "LA Tools"
    bl_label = "Clears the keyframelist"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"

    def execute(self, context):
        keyframes.clear()
        return {'FINISHED'}

class SetCurrentFrameToKeyframesCycle(bpy.types.Operator):
    '''Cycles through the keyframes in the list and sets the current frame to the keyframe'''
    bl_idname = "setframe.keyframe"
    bl_category = "LA Tools"
    bl_label = "Sets the current frame to the next keyframe in the list"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    frames = []

    def execute(self, context):
        frame_end = bpy.context.scene.frame_end
        frame_start = bpy.context.scene.frame_start
        for i in range(frame_start, frame_end + 1, 1):
            self.frames.append(i)

        if keyframes:                                                   # If keyframes-list isn't empty, continue
            if bpy.context.scene.frame_current not in keyframes:        # Check if current frame is not in keyframes
                    bpy.context.scene.frame_current = keyframes[0]      # Set current frame to the 1st key in keyframes
            else:
                for i in range(0, len(keyframes)):                      # Goes through all the keys in keyframes
                    if bpy.context.scene.frame_current == keyframes[i]:
                        try:
                            bpy.context.scene.frame_current = keyframes[i+1]    # Go to next key in keyframes
                            break                                               # Stop the loop
                            return {'FINISHED'}
                        except IndexError:
                            bpy.context.scene.frame_current = keyframes[0]      # If we reached the end go to first key
                            return {'FINISHED'}
                    else:
                        continue                                     # if the key isn't the current keyframe - continue
        return {'FINISHED'}

    # How it should be drawn:
    """
        def draw(self, context):
            scn = bpy.context.scene
            layout = self.layout
            layout.label(text="Keyframes: " + str(keyframes), icon="UV_EDGESEL")
            row = layout.row()
            row = layout.row(align=True)
            row.operator(AddKeyframes.bl_idname, "Add")
            row.operator(RemoveKeyframes.bl_idname, "Remove")
            row = layout.row(align=True)
            row.operator(ClearKeyframes.bl_idname, "Clear list")
            row.operator(SetCurrentFrameToKeyframesCycle.bl_idname, "Cycle keyframes")
            row = layout.row(align=True)
            row.operator(AnimExtractor.bl_idname, "Extract from selected", icon="POSE_HLT")
            if not keyframes:  # check if list isn't empty
                row.enabled = False  # gray out button
            row = layout.row()
            row = layout.row()
    """