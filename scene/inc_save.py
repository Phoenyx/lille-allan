

__author__ = 'render'
import re
import os
import shutil
import getpass

import bpy
from utils.script_utils import prompt
from utils.script_utils import ANIM_FILE_NAME_REGEX_NAME
from utils.script_utils import ANIM_FILE_NAME_REGEX_VERSION
from utils.script_utils import PROP_FILE_NAME_REGEX_VERSION
from utils.script_utils import PROP_FILE_NAME_REGEX_NAME


class IncrementFileVersion(bpy.types.Operator):
    '''Saves the scene and moves it to the Inc.folder. Then increments the version number of the scene and saves it'''
    bl_idname = "la.inc_save"
    bl_label = "Incremental Saved"

    PROP_NAME = PROP_FILE_NAME_REGEX_NAME
    PROP_VERSION = PROP_FILE_NAME_REGEX_VERSION
    ANIM_NAME = ANIM_FILE_NAME_REGEX_NAME
    ANIM_VERSION = ANIM_FILE_NAME_REGEX_VERSION

    # Function which runs when button is clicked
    def execute(self, context):
        filename = self.get_current_filename()
        is_prop_scene = bool

        if self.filename_is_valid(filename):
            curr_file_path = bpy.data.filepath
            bpy.ops.wm.save_as_mainfile(filepath=curr_file_path)
            new_file_path = self.increment_file_version(filename, curr_file_path, is_prop_scene)
            if not os.path.exists(new_file_path):
                self.move_to_inc_folder(curr_file_path, filename)
                self.save_file(new_file_path)
                # Test the name of the file
                # Copy the current file into the increment folder
                # Increment the current file name
                # Save
                prompt("Scene Incremental Saved!")  # FEEDBACK TO USER
                return {'FINISHED'}
        else:
            prompt("Wrong Naming of Scene file!", icon="ERROR")  # FEEDBACK TO USER
            self.report({'WARNING', 'INFO'}, "Wrong Naming of Scene file")
            return {'FINISHED'}

    def get_current_filename(self):
        scene_file_name_ext = os.path.splitext(bpy.path.basename(bpy.context.blend_data.filepath))
        return str(scene_file_name_ext[0])

    def filename_is_valid(self, filename):
        if re.match(self.PROP_NAME, filename):
            self.is_prop_scene = True
            return True, self.is_prop_scene

        elif re.match(self.ANIM_NAME, filename):
            self.is_prop_scene = False
            return True, self.is_prop_scene

        else:
            return False

    def increment_file_version(self, scene_file_name, filepath, is_prop_scene):
        if self.is_prop_scene:
            matches = re.search(self.PROP_VERSION, scene_file_name)
            groups = matches.groups()
            current_version = groups[2]

            # Check if version number is below 999
            if current_version == "999":
                new_version = "000"
            else:
                new_version = "%03d" % (int(current_version) + 1)  # adds 0's in front of integer if necessary

            # Updates filename with new version number and user initials
            new_scene_name = scene_file_name.replace(groups[2], new_version)
            new_scene_name = new_scene_name.replace(groups[3], self.get_user_initials())
            return filepath.replace(scene_file_name, new_scene_name)

        else:
            if not self.is_prop_scene:
                matches = re.search(self.ANIM_VERSION, scene_file_name)
                groups = matches.groups()
                current_version = groups[2]

                # Check if version number is below 999
                if current_version == "999":
                    new_version = "000"
                else:
                    new_version = "%03d" % (int(current_version) + 1)  # adds 0's in front of integer if necessary

                # Updates filename with new version number and user initials
                # replaces once from the right - this method is necessary due to the multiple occurrences of digits
                new_scene_name = str(new_version.join(scene_file_name.rsplit(current_version, 1)))
                new_scene_name = new_scene_name.replace(groups[3], self.get_user_initials())

                return filepath.replace(scene_file_name, new_scene_name)

    def move_to_inc_folder(self, curr_file_path, filename):

        main_folder = os.path.dirname(curr_file_path)

        inc_folder = str(main_folder + "\IncrementalSave")
        inc_path = inc_folder + "/" + filename + ".blend"
        auto_save = curr_file_path + "1"
        auto_save_inc_path = inc_path + "1"

        if not os.path.exists(inc_folder):
            self.report({'INFO'}, "Creating folder " + inc_folder)
            os.makedirs(inc_folder)  # make incremental save folder if it doesnt already exists

        shutil.move(curr_file_path, inc_path)  # move the old file to the incremental folder

        if os.path.exists(auto_save):
            shutil.move(auto_save, auto_save_inc_path)  # move the .blend1 file to the incremental folder


    def get_user_initials(self):
        user = getpass.getuser()
        return str(user[0] + user[-1]).lower()

    def save_file(self, new_file_path):
        try:
            bpy.ops.wm.save_as_mainfile(filepath=new_file_path)
            self.report({'INFO'}, "Save as " + new_file_path)
        except:
            self.report({'WARNING'}, {'INFO'}, "Error saving as " + new_file_path)
