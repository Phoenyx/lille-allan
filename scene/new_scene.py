__author__ = 'render'
import os

import bpy
import re
from utils.script_utils import prompt
from scene.inc_save import IncrementFileVersion
from utils.script_utils import ANIM_FILE_NAME_REGEX_NAME
from utils.script_utils import ANIM_FILE_NAME_REGEX_VERSION
from utils.script_utils import PROP_FILE_NAME_REGEX_VERSION
from utils.script_utils import PROP_FILE_NAME_REGEX_NAME
from utils.script_utils import animation_folder
from utils.script_utils import render_folder
from utils.script_utils import asset_folder


class NewAssetScene(bpy.types.Operator):
    '''Creates a new blender scene with the given name and of the given type'''
    bl_idname = "asset.scene"
    bl_label = "New Asset Scene"

    PROP_NAME = PROP_FILE_NAME_REGEX_NAME
    PROP_VERSION = PROP_FILE_NAME_REGEX_VERSION
    ANIM_NAME = ANIM_FILE_NAME_REGEX_NAME
    ANIM_VERSION = ANIM_FILE_NAME_REGEX_VERSION
    ASSET_FOLDER = asset_folder
    RENDER_FOLDER = render_folder
    ANIMATION_FOLDER = animation_folder

    def execute(self, context):
        scn = bpy.context.scene

        # Check that type and asset name have been inputted
        try:
            if scn['MyEnum'] == 0 or scn['AssetName'] == "":
                # No type selected
                return {'FINISHED'}
        except KeyError:
            prompt("Please choose type and name", icon="ERROR")
            return {'FINISHED'}

        if scn['MyEnum'] == 1:
            asset_type = "character"

        if scn['MyEnum'] == 2:
            asset_type = "prop"

        if scn['MyEnum'] == 3:
            asset_type = "animation"

        if scn['MyEnum'] == 4:
            asset_type = "render"

        scene_name = str(scn['AssetName'])

        initials = str(IncrementFileVersion.get_user_initials(self))

        tmp_name = str(scene_name) + "_v001_" + str(initials)

        filename = scene_name + "_v001_" + initials + ".blend"

        # name fits either prop or anim scene convention
        if IncrementFileVersion.filename_is_valid(self, tmp_name):
            print("Continue")

            # name fits animation scene convention
            if (asset_type == "animation" or asset_type == "render") and re.match(self.ANIM_NAME, tmp_name):
                pass

            # name fits prop scene convention
            elif (asset_type == "prop" or asset_type == "character") and re.match(self.PROP_NAME, tmp_name):
                pass

            # name doesn't fit any convention
            else:
                prompt("Illegal Naming", icon="ERROR")
                return {'FINISHED'}
        else:
            # name doesn't fit any convention
            prompt("Illegal Naming", icon="ERROR")
            return {'FINISHED'}

        if asset_type == "character" or asset_type == "prop":
            # file path to the new asset's folder
            filepath = self.ASSET_FOLDER + asset_type + "\\" + scene_name

            if not os.path.exists(filepath):
                self.report({'INFO'}, "Creating folder :" + filepath)
                folders = ["\\rig", "\sourceImages", "\\animLink", "\export", "\\vfx", "\model"]  # model should always be last
                for folder in folders:  # Creates all folders above in the asset's folder
                    filepath = self.ASSET_FOLDER + asset_type + "\\" + scene_name + folder
                    os.makedirs(filepath)

                path = str(filepath + "\\" + filename)
                bpy.ops.wm.read_homefile()  # Opens the user's start up file
                bpy.ops.wm.save_as_mainfile(filepath=path)  # Saves the scene

            else:
                prompt("Asset already exists", icon="ERROR")

        elif asset_type == "animation" or asset_type == "render":

            SQ, SC, SH = scene_name.split('_')

            if asset_type == "animation":
                filepath = self.ANIMATION_FOLDER + "\\" + SQ + "\\" + SC + "\\" + SH + "\\"     # Scene parent folder
            else:
                filepath = self.RENDER_FOLDER + "\\" + SQ + "\\" + SC + "\\" + SH + "\\"

            if not os.path.exists(filepath):                    # Check to see if path already exists
                folders = ["\export", "\IncrementalSave"]      # Folders to create inside animation/render scene folder

                for folder in folders:  # Creates all folders above
                    folder_path = filepath + folder
                    os.makedirs(folder_path)

                path = str(filepath + "\\" + filename)
                bpy.ops.wm.read_homefile()  # Opens the user's start up file
                bpy.ops.wm.save_as_mainfile(filepath=path)  # Saves the scene

            else:
                prompt("Scene already exists", icon="ERROR")

        else:
            prompt("Type error", icon="ERROR")

        return {'FINISHED'}
