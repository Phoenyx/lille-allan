__author__ = 'render'
import bpy

from utils.clear_scale import ClearScale
from utils.anim_link import MakeAnimLink
from utils.ref_obj import ImportReferenceCube


class ModelPanel(bpy.types.Panel):
    bl_idname = "model.panel"
    bl_category = "LA Tools"
    bl_label = "Model/Rig Scene Tools"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"

    def draw_header(self, context):
        layout = self.layout
        layout.label(text="", icon="MESH_ICOSPHERE")

    def draw(self, context):
        layout = self.layout
        split = layout.split()
        col = split.column(align=True)
        col.operator(ClearScale.bl_idname, text="Freeze object scale", icon="FREEZE")
        row = layout.row()
        row.operator(MakeAnimLink.bl_idname, text="Create anim link file", icon="LINK_BLEND")
        row = layout.row()
        row.operator(ImportReferenceCube.bl_idname, text="Import 1cm cube", icon="MESH_CUBE")
