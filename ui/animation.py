__author__ = 'render'

import bpy
from animation.playblast import CreatePlayblast
from animation.camera import CamExportFBX, CreateMainCam
from utils.obj_export import ExportAllObj, ExportSelectedObj
from utils.mdd_export import ExportAllMdd, ExportSelectedMdd
from animation.intersecting_geo import IntersectCheck


class AnimationPanel(bpy.types.Panel):
    bl_idname = "anim.panel"
    bl_category = "LA Tools"
    bl_label = "Animation Scene Tools"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"

    def draw_header(self, context):
        layout = self.layout
        layout.label(text="", icon="POSE_DATA")

    def draw(self, context):
        scn = bpy.context.scene
        layout = self.layout

        row = layout.row()
        row.operator(CreateMainCam.bl_idname, text="Create main camera", icon="OUTLINER_OB_CAMERA")
        row = layout.row()
        row.operator(CreatePlayblast.bl_idname, text="Playblast", icon="RENDER_ANIMATION")
        row = layout.row()
        row.operator(IntersectCheck.bl_idname, text="Check intersections", icon="IPO")
        row = layout.row()

        layout.label(text="Export tools", icon="EXPORT")
        row = layout.row()
        row.operator(CamExportFBX.bl_idname, text="Camera as .fbx", icon="CAMERA_DATA")
        row = layout.row()
        row = layout.row(align=True)
        row.operator(ExportAllMdd.bl_idname, text="All as .mdd")
        row.operator(ExportSelectedMdd.bl_idname, text="Selected as .mdd")
        row = layout.row(align=True)
        row.operator(ExportAllObj.bl_idname, text="All as .obj")
        row.operator(ExportSelectedObj.bl_idname, text="Selected as .obj")
        row = layout.row()

