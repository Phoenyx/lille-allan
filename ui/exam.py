__author__ = 'render'
import bpy
from bpy.app.handlers import persistent
import os
from render.light_import import ImportLightsFromList
from render.light_export_blender import ExportLightsBlender
from render.eye_light_rig import EyeLightRig
from animation.intersecting_geo import IntersectCheck


class LightList(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_label = "Exam Tools"
    bl_idname = "lights.list"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = "LA Tools"
    data = None
    filtered_data = None

    @persistent
    def path(self):
        # render        # SQ            # SC            # SH            # .blend file
        render_folder_path = (os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(bpy.data.filepath)))))
        return render_folder_path

    @persistent
    def in_render_scene(self):
        if "render" in bpy.data.filepath:
            return True

    @classmethod
    def poll(self, context):
        return True

    def draw_header(self, context):
        layout = self.layout
        layout.label(text="", icon="COLOR_BLUE")

    def sq_folders(self, context):
        return LightList.filtered_data[0]

    def sc_folders(self, context):
        return LightList.filtered_data[1]

    def light_files(self, context):
        return LightList.filtered_data[2]

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.operator(IntersectCheck.bl_idname, text="Check intersections", icon="MOD_ARMATURE")
        row = layout.row()
        row.operator(ExportLightsBlender.bl_idname, text="Export all lights", icon="COPYDOWN")

        if LightList.in_render_scene(self):

            if LightList.data is None:
                LightList.data = LightList.get_light_data(self)
                LightList.filtered_data = LightList.get_filtered_data(self, LightList.data)

            layout.label("Choose light by filtering SQ and SC ")
            split1 = layout.split()
            col = split1.column()
            sub = col.column(align=True)
            sub.prop(context.window_manager, "sq_folders")
            sub.prop(context.window_manager, "sc_folders")
            sub.prop(context.window_manager, "light_files")
            row = layout.row()

            split = row.split()
            col = split.column(align=True)
            split = col.split()
            split.label("Import light file")
            split.operator(ImportLightsFromList.bl_idname, text="Import", icon="PASTEDOWN")
        else:
            layout.label("Cannot import lights: ")
            layout.label("Not currently in a render scene")

        row = layout.row()
        row = layout.row()
        row.operator(EyeLightRig.bl_idname, text="Set up eye light rig", icon="LAMP_HEMI")
        row = layout.row()

    def get_light_data(self):
        output = {}
        path = LightList.path(self)

        # looks at the content in the render directory and adds them to the list if they are directories
        sequence_folders = [f for f in os.listdir(path) if os.path.isdir(os.path.join(path, f))]

        for sequence_folder in sequence_folders:
            output[sequence_folder] = {}
            sequence_path = os.path.join(path, sequence_folder)

            # looks at the content in the sequences directories and adds them to the list if they are directories
            scene_folders = [f for f in os.listdir(sequence_path) if os.path.isdir(os.path.join(sequence_path, f))]

            for scene_folder in scene_folders:
                scene_path = os.path.join(sequence_path, scene_folder)

                # looks at the content in the scene directories and adds them to the list if they are directories
                shot_folders = [f for f in os.listdir(scene_path) if os.path.isdir(os.path.join(scene_path, f))]

                for shot_folder in shot_folders:
                    export_path = os.path.join(scene_path, shot_folder, "export")
                    try:
                        fbx_lights = [f for f in os.listdir(export_path) if
                                      "Lights.fbx" in f]

                        if scene_folder in output[sequence_folder] is not None:     # check if there is already a light attached to this scene
                            output[sequence_folder][scene_folder].extend(fbx_lights)    # extends the list of lights to include this one too
                        else:
                            output[sequence_folder][scene_folder] = fbx_lights

                    except WindowsError:
                        pass
        return output

    def get_filtered_data(self, data, sequence_filter=None, scene_filter=None):
        sequences = []
        scenes = []
        fbx_lights = []

        for sequence in data.keys():
            sequences.append((sequence, sequence, ""))      # Add all the sequences, unfiltered

        # key           # value        # returns the keys and values
        for sequence, sequence_dict in data.items():

            if sequence_filter is None or sequence_filter in sequence:      # if the filter is in the key

                for scene, scene_dict in sequence_dict.items():
                    scenes.append((scene, scene, ""))           # Add the filtered scenes

                    if scene_filter is None or scene_filter in scene:       # if the filter is in the key
                        for fbx_light in scene_dict:

                            fbx_lights.append((fbx_light, fbx_light, ""))   # Add the filtered lights

        return sequences, scenes, fbx_lights

    def update_data(self, context):
        '''
        Function used to update the lists used by the dropdown menues.
        '''
        sequence = bpy.data.window_managers["WinMan"].sq_folders
        scene = bpy.data.window_managers["WinMan"].sc_folders

        data = LightList.get_light_data(self)
        # Run the function with the new filters
        LightList.filtered_data = LightList.get_filtered_data(self, data, sequence_filter=sequence, scene_filter=scene)

