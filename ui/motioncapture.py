__author__ = 'render'

import bpy
import os

from motioncapture.motioncapture_file import MakeAnimReadyMotionCaptureFile

class RefreshList(bpy.types.Operator):
    bl_label = "Refresh List"
    bl_idname = "scene.object_lib_refresh"

    def execute(self, context):
        ShowBvhList.update = True
        ShowBvhList.refreshed = False
        ShowBvhList.bvh_files = sorted(ShowBvhList.bvh_files)
        return {'FINISHED'}


class ShowBvhList(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_label = "Mocap Tools"
    bl_idname = "bvh.list"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = "LA Tools"

    update = True
    refreshed = False
    obs = []

    mocap_path = r"M:\projects\Lille Allan\TeaserProduction\02_production\mocap"

    bvh_files = []

    def draw_header(self, context):
        layout = self.layout
        layout.label(text="", icon="OUTLINER_DATA_ARMATURE")

    def draw(self, context):
        layout = self.layout
        layout.prop(context.window_manager, "bvh_files")
        if self.__class__.refreshed:
            layout.label("Error, refresh list, try again.", icon='INFO')
        layout.operator(RefreshList.bl_idname)
        row = layout.row()
        row.operator(MakeAnimReadyMotionCaptureFile.bl_idname, text="Start process")

    def update_cb(self, context):
        new_obs = [ob for ob in context.scene.objects if ob.select]
        if len(new_obs) > 0:
            context.scene.objects.active = new_obs[0]
            ShowBvhList.refreshed = False
        else:
            bpy.ops.scene.object_lib_refresh()
            ShowBvhList.refreshed = True

    def file_list(self, context):
        ShowBvhList.bvh_files = []
        for (dirpath, dirnames, filenames) in os.walk(ShowBvhList.mocap_path):
            for file in filenames:
                if file.endswith(".bvh") and not file.startswith("scaled_"):
                    ShowBvhList.bvh_files.append((file, file, ''))
        return sorted(ShowBvhList.bvh_files)