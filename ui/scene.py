__author__ = 'render'

import bpy
from scene.inc_save import IncrementFileVersion
from scene.new_scene import NewAssetScene
from utils.update import UpdateScripts
from utils.settings import SceneSettings


class ScenePanel(bpy.types.Panel):
    bl_idname = "ascene.panel"
    bl_category = "LA Tools"
    bl_label = "General Tools"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"

    show_help = False

    def version_number(self):
        version = "Vrs.: " + "0.2.0"
        return version

    """def SceneChanged(self):
        global last_value
        if bpy.data.is_dirty != last_value:
            last_value = bpy.data.is_dirty
            for area in bpy.context.screen.areas:
                if area.type == ScenePanel.bl_space_type:
                    for region in area.regions:
                        if region.type == ScenePanel.bl_region_type:
                            region.tag_redraw()"""

    def draw_header(self, context):
        layout = self.layout
        layout.label(text="", icon="SCENE_DATA")

    def draw(self, context):
        layout = self.layout
        box = layout.box()
        row = box.row(align=True)
        split = row.split()
        col = split.column(align=True)
        split = col.split()
        split.label(str(self.version_number()))
        split.operator(UpdateScripts.bl_idname, text="Update", icon="FILE_REFRESH")
        row = layout.row()
        row.operator(IncrementFileVersion.bl_idname, text="Incremental save", icon="SAVE_COPY")
        row = layout.row()
        row = layout.row()
        scn = bpy.context.scene
        layout.prop(scn, 'AssetName')
        row = layout.row()
        layout.prop(scn, 'MyEnum')
        row = layout.row()
        scn = context.scene
        layout.prop(scn, 'ShowHelpBool')
        row = layout.row()

        if bpy.context.scene.ShowHelpBool:
            layout.label("Examples")
            layout.label("Prop/Character:")
            layout.label("'LittleAllan', 'Chair01', 'Book02' ")
            row = layout.row()
            layout.label("Animation/Render:")
            layout.label("'SQ0010_SC0010_SH0010' ")

        row = layout.row()
        row.operator(NewAssetScene.bl_idname, text="Create new scene", icon="BLENDER")
        row = layout.row()
        row.operator(SceneSettings.bl_idname, text="Set scene settings", icon="SCRIPTWIN")
        row = layout.row()

