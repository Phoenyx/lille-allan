__author__ = 'render'
import bpy


class InfoPanel(bpy.types.Panel):
    bl_idname = "info.panel"
    bl_category = "LA Tools"
    bl_label = "Info"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"

    def draw_header(self, context):
        layout = self.layout
        layout.label(text="", icon="INFO")

    def draw(self, context):
        layout = self.layout
        layout.label(" Lille Allan Production Tools ")
        layout.label("Please report errors to Sofia:")
        layout.label("  sofiaelenajm@gmail.com  ")
        row = layout.row()