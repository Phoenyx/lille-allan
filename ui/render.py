__author__ = 'render'
import bpy
from render.cache import AddClothCache, AddMDDCacheToAll
from render.light_export_houdini import ExportLights
from render.obj_import import ImportAllObj
from render.camera_import import ImportCamera
from render.rendering_settings import SetRenderingSettings, MakePathsRelative

from render.light_import import ImportLightsFromList
from render.light_export_blender import ExportLightsBlender
from render.eye_light_rig import EyeLightRig
from bpy.app.handlers import persistent


class RenderPanel(bpy.types.Panel):
    bl_idname = "render.panel"
    bl_category = "LA Tools"
    bl_label = "Render Scene Tools"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"

    def draw_header(self, context):
        layout = self.layout
        layout.label(text="", icon="RENDER_RESULT")

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.operator(SetRenderingSettings.bl_idname, text="Set default render settings", icon="SCRIPTWIN")
        row = layout.row()
        row.operator(MakePathsRelative.bl_idname, text="Relative paths", icon="SCRIPTWIN")

        row = layout.row()
        col = layout.column(align=True)
        col.label("From anim and VFX:", icon="MOD_PHYSICS")
        col.operator(ImportCamera.bl_idname, text="Import camera", icon="OUTLINER_DATA_CAMERA")
        col.operator(ImportAllObj.bl_idname, text="Import all objs", icon="MOD_OCEAN")
        col.operator(AddMDDCacheToAll.bl_idname, text="Add cache to all", icon="MODIFIER")
        col.operator(AddClothCache.bl_idname, text="Add cloth", icon="MOD_CLOTH")
        row = layout.row()
        row.operator(EyeLightRig.bl_idname, text="Create eye light rig", icon="LAMP_HEMI")
        row = layout.row()
        col = layout.column(align=True)
        col.label("Export lights:", icon="LAMP_SPOT" )
        col.operator(ExportLights.bl_idname, text="Selected: Houdini")
        col.operator(ExportLightsBlender.bl_idname, text="All: Blender")
        row = layout.row()

        if RenderPanel.in_render_scene(self):

            layout.label("Choose light by filtering SQ and SC ")
            split1 = layout.split()
            col = split1.column()
            sub = col.column(align=True)
            sub.prop(context.window_manager, "sq_folders")
            sub.prop(context.window_manager, "sc_folders")
            sub.prop(context.window_manager, "light_files")
            row = layout.row()

            split = row.split()
            col = split.column(align=True)
            split = col.split()
            split.label("Import light file")
            split.operator(ImportLightsFromList.bl_idname, text="Import", icon="PASTEDOWN")
        else:
            layout.label("Cannot import lights: ")
            layout.label("Not currently in a render scene")

    @persistent
    def in_render_scene(self):
        if "render" in bpy.data.filepath:
            return True



