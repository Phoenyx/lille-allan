import sys
import os

import bpy


# Add the current addon folder to the PythonPath
sys.path.append(os.path.dirname(__file__))

bl_info = {
    "name": "Lille Allan Menu",
    "category": "Lille Allan",
    "author": "Sofia Elena Jakobsen Manioudakis"
}

# To check if scene is dirty
last_value = False

""" __ Import External Modules __ : """
from bpy.props import *

from animation.link_char import LinkRiggedChars
from animation.playblast import CreatePlayblast
from animation.key_extractor import RemoveKeyframes, AddKeyframes, AnimExtractor, SetCurrentFrameToKeyframesCycle, \
    ClearKeyframes
from animation.camera import CreateMainCam, CamExportFBX
from animation.intersecting_geo import IntersectCheck

from motioncapture.motioncapture_file import MakeAnimReadyMotionCaptureFile

from render.light_export_houdini import ExportLights
from render.light_export_blender import ExportLightsBlender

from render.obj_import import ImportAllObj
from render.cache import AddMDDCacheToAll, AddClothCache
from render.camera_import import ImportCamera
from render.rendering_settings import SetRenderingSettings, MakePathsRelative
from render.light_import import ImportLightsFromList
from render.eye_light_rig import EyeLightRig

from scene.inc_save import IncrementFileVersion
from scene.new_scene import NewAssetScene
from utils.anim_link import MakeAnimLink

from utils.mdd_export import ExportAllMdd, ExportSelectedMdd
from utils.obj_export import ExportAllObj, ExportSelectedObj
from utils.clear_scale import ClearScale
from utils.update import UpdateScripts
from utils.settings import SceneSettings
from utils.ref_obj import ImportReferenceChair, ImportReferenceCube
from utils.props_init import InitMyPropOperator

import motioncapture.fix_framerate as meh
from motioncapture.fix_framerate import FixMocapFramerate
import imp
imp.reload(meh)
""" _________ UI __________ """

from ui.scene import ScenePanel
from ui.animation import AnimationPanel
from ui.model import ModelPanel
from ui.render import RenderPanel
from ui.exam import LightList
from ui.motioncapture import ShowBvhList, RefreshList
from ui.info import InfoPanel

""" _________ Registering __________ """


def register():
    bpy.utils.register_class(EyeLightRig)
    bpy.utils.register_class(ExportLightsBlender)
    bpy.utils.register_class(ImportLightsFromList)
    bpy.utils.register_class(IntersectCheck)
    bpy.utils.register_class(MakePathsRelative)
    bpy.utils.register_class(SetRenderingSettings)
    bpy.utils.register_class(FixMocapFramerate)
    bpy.utils.register_class(MakeAnimReadyMotionCaptureFile)
    bpy.utils.register_class(ImportCamera)
    bpy.utils.register_class(RefreshList)
    bpy.utils.register_class(UpdateScripts)
    bpy.utils.register_class(NewAssetScene)
    bpy.utils.register_class(MakeAnimLink)
    bpy.utils.register_class(IncrementFileVersion)
    bpy.utils.register_class(ExportAllMdd)
    bpy.utils.register_class(ExportSelectedMdd)
    bpy.utils.register_class(AddMDDCacheToAll)
    bpy.utils.register_class(AddClothCache)
    bpy.utils.register_class(ClearScale)
    bpy.utils.register_class(SceneSettings)
    bpy.utils.register_class(ImportAllObj)
    bpy.utils.register_class(ExportAllObj)
    bpy.utils.register_class(ExportSelectedObj)
    bpy.utils.register_class(ImportReferenceCube)
    bpy.utils.register_class(ImportReferenceChair)
    bpy.utils.register_class(LinkRiggedChars)
    bpy.utils.register_class(AnimExtractor)
    bpy.utils.register_class(RemoveKeyframes)
    bpy.utils.register_class(AddKeyframes)
    bpy.utils.register_class(ClearKeyframes)
    bpy.utils.register_class(SetCurrentFrameToKeyframesCycle)
    bpy.utils.register_class(CreateMainCam)
    bpy.utils.register_class(CamExportFBX)
    bpy.utils.register_class(CreatePlayblast)
    bpy.utils.register_class(ExportLights)

    bpy.utils.register_class(ScenePanel)
    bpy.utils.register_class(AnimationPanel)
    bpy.utils.register_class(ModelPanel)
    bpy.utils.register_class(RenderPanel)
    bpy.utils.register_class(LightList)
    bpy.utils.register_class(InfoPanel)

    bpy.utils.register_class(InitMyPropOperator)

    bpy.types.Scene.AssetName = bpy.props.StringProperty(default="Enter Scene Name", name="Name")
    bpy.types.Scene.MyEnum = EnumProperty(
        items=[('Type', 'Type', 'Type'),
               ('Char', 'Char', 'Char'),
               ('Prop', 'Prop', 'Prop'),
               ('Animation', 'Animation', 'Animation'),
               ('Render', 'Render', 'Render')],
        name="Type")

    bpy.types.Scene.Examples = EnumProperty(
        items=[('Char/Prop', 'Char/Prop', 'Char/Prop'),
               ('Anim/Render', 'Anim/Render', 'Anim/Render')],
        name="Examples")

    bpy.types.Scene.CharEnum = EnumProperty(
        items=[('Lille Allan', 'Lille Allan', 'Lille Allan')],
        name="Char")

    bpy.types.Scene.ShowHelpBool = bpy.props.BoolProperty(name="Show name help",
                                                      description="Hide/Show examples for naming props/chars and anim/render scenes",
                                                      default=False)

    bpy.types.WindowManager.bvh_files = bpy.props.EnumProperty(items=ShowBvhList.file_list, update=ShowBvhList.update_cb)

    bpy.types.WindowManager.sq_folders = bpy.props.EnumProperty(items=LightList.sq_folders, update=LightList.update_data)
    bpy.types.WindowManager.sc_folders = bpy.props.EnumProperty(items=LightList.sc_folders, update=LightList.update_data)
    bpy.types.WindowManager.light_files = bpy.props.EnumProperty(items=LightList.light_files, update=LightList.update_data)


def unregister():
    bpy.utils.unregister_class(EyeLightRig)
    bpy.utils.unregister_class(ExportLightsBlender)
    bpy.utils.unregister_class(ImportLightsFromList)
    bpy.utils.unregister_class(IntersectCheck)
    bpy.utils.unregister_class(MakePathsRelative)
    bpy.utils.unregister_class(SetRenderingSettings)
    bpy.utils.unregister_class(FixMocapFramerate)
    bpy.utils.unregister_class(MakeAnimReadyMotionCaptureFile)
    bpy.utils.unregister_class(ImportCamera)
    bpy.utils.unregister_class(RefreshList)
    bpy.utils.unregister_class(UpdateScripts)
    bpy.utils.unregister_class(NewAssetScene)
    bpy.utils.unregister_class(MakeAnimLink)
    bpy.utils.unregister_class(IncrementFileVersion)
    bpy.utils.unregister_class(ExportAllMdd)
    bpy.utils.unregister_class(ExportSelectedMdd)
    bpy.utils.unregister_class(AddMDDCacheToAll)
    bpy.utils.unregister_class(AddClothCache)
    bpy.utils.unregister_class(ClearScale)
    bpy.utils.unregister_class(SceneSettings)
    bpy.utils.unregister_class(ImportAllObj)
    bpy.utils.unregister_class(ExportAllObj)
    bpy.utils.unregister_class(ExportSelectedObj)
    bpy.utils.unregister_class(ImportReferenceCube)
    bpy.utils.unregister_class(ImportReferenceChair)
    bpy.utils.unregister_class(LinkRiggedChars)
    bpy.utils.unregister_class(AnimExtractor)
    bpy.utils.unregister_class(RemoveKeyframes)
    bpy.utils.unregister_class(AddKeyframes)
    bpy.utils.unregister_class(ClearKeyframes)
    bpy.utils.unregister_class(SetCurrentFrameToKeyframesCycle)
    bpy.utils.unregister_class(CreateMainCam)
    bpy.utils.unregister_class(CamExportFBX)
    bpy.utils.unregister_class(CreatePlayblast)
    bpy.utils.unregister_class(ExportLights)

    bpy.utils.unregister_class(InitMyPropOperator)

    bpy.utils.unregister_class(ScenePanel)
    bpy.utils.unregister_class(AnimationPanel)
    bpy.utils.unregister_class(ModelPanel)
    bpy.utils.unregister_class(RenderPanel)
    bpy.utils.unregister_class(LightList)
    bpy.utils.unregister_class(InfoPanel)

    del bpy.types.WindowManager.bvh_files
    del bpy.types.WindowManager.light_files
    del bpy.types.WindowManager.sq_folders
    del bpy.types.WindowManager.sc_folders

    del bpy.types.Scene.Examples
    del bpy.types.Scene.AssetName
    del bpy.types.Scene.MyEnum
    del bpy.types.Scene.ShowHelpBool

if __name__ == "__main__":
    register()
