__author__ = 'render'
import os
import re

import bpy
from utils.script_utils import prompt
from utils.script_utils import ANIM_FILE_NAME_REGEX_NAME


class ExportLights(bpy.types.Operator):
    '''Exports all the light objects in the scene'''
    bl_idname = "light.exp"
    bl_label = "Export all the light objects in the scene"

    ANIM_NAME = ANIM_FILE_NAME_REGEX_NAME

    def execute(self, context):

        scene = bpy.context.scene

        scene_file_name_ext = os.path.splitext(bpy.path.basename(bpy.context.blend_data.filepath))
        filename = str(scene_file_name_ext[0])
        curr_file_path = bpy.data.filepath
        main_folder = os.path.dirname(curr_file_path)
        export_folder = main_folder + "\export\\"

        if re.match(self.ANIM_NAME, filename):
            matches = re.search(self.ANIM_NAME, filename)
            groups = matches.groups()
            filename = groups[0]
        else:
            prompt("Failed to export - check scene naming.")
            return {'FINISHED'}

        if not os.path.exists(export_folder):
            print("Creating {0}".format(export_folder))
            os.makedirs(export_folder)

        write_string = ''

        for obj in bpy.context.selected_objects:
            obj_type = obj.type
            if obj_type == 'LAMP':
                data = obj.data
                obj.select = True
                name = obj.name
                light_type = obj.data.type
                shadows = str(int(obj.data.cycles.cast_shadow))
                intensity = str((data.node_tree.nodes["Emission"].inputs[1].default_value / 100) * 3)
                color = (str(data.node_tree.nodes["Emission"].inputs[0].default_value[0]) + ',' +
                         str(data.node_tree.nodes["Emission"].inputs[0].default_value[1]) + ',' +
                         str(data.node_tree.nodes["Emission"].inputs[0].default_value[2]))

                cone_angle = '0'
                if light_type == 'SPOT':
                    cone_angle = str(round(obj.data.spot_size * 57.2957795, 2))

                write_string += name + ',' + light_type + ',' + intensity + ',' + color + ',' + cone_angle + ',' + shadows + '\r\n'

        try:
            hdr_orientation = str(-round(bpy.data.worlds['World'].node_tree.nodes['Environment Texture'].texture_mapping.rotation[2] * 57.2957795, 2) + 90)
        except:
             hdr_orientation = "0"

        write_string += hdr_orientation + '\r\n'

        export_path_string = export_folder + filename + "_lights.txt"
        export_path = export_folder + filename + "_lights.fbx"

        if len(bpy.context.selected_objects) >= 1:

            f = open(export_path_string, 'w')
            f.write(write_string)
            f.close()

            bpy.ops.export_scene.fbx(filepath=export_path, filter_glob="*.fbx", axis_forward='Y', axis_up='Z',
                                     bake_anim_use_nla_strips=False, bake_anim_use_all_actions=False,
                                     use_selection=True)

            prompt("all selected lights exported to " + export_path)

        else:
            prompt("No lights selected")  # FEEDBACK TO USER

        return {'FINISHED'}