__author__ = 'render'
import os
import re

import bpy
from utils.script_utils import prompt
from render.obj_import import ImportAllObj
from utils.script_utils import ANIM_FILE_NAME_REGEX_NAME
from utils.script_utils import MDD_FILE_NAME_REGEX


class AddMDDCacheToAll(bpy.types.Operator):
    """ Add an Mesh Cache Modifier to all objs in the scene and add MDD cache into it """
    bl_idname = "addmdd.cache"
    bl_label = "Adds cache to selected objects automatically"

    ANIM_NAME = ANIM_FILE_NAME_REGEX_NAME
    MDD_NAME = MDD_FILE_NAME_REGEX

    def execute(self, context):
        bpy.ops.object.select_all(action='DESELECT')
        bpy.ops.object.select_pattern(pattern="*_Mesh")
        r = bpy.context.selected_objects

        for obj in r:
            bpy.ops.object.select_hierarchy(direction='CHILD', extend=False)
            bpy.ops.object.select_hierarchy(direction='CHILD', extend=True)
            break

        for child in bpy.context.selected_objects:
            if re.match(self.MDD_NAME, child.name):
                matches = re.search(self.MDD_NAME, child.name)
                groups = matches.groups()
                mdd_file_name = str(groups[0]) + ".mdd"

                bpy.ops.object.select_all(action='DESELECT')
                bpy.ops.object.select_pattern(pattern=child.name)
                print(child.name)
                bpy.context.scene.objects.active = child

                if 'MESH_CACHE' not in (mod.type for mod in child.modifiers):  # Check to see if obj

                    bpy.ops.object.modifier_add(type='MESH_CACHE')  # already has a cache modifier

                bpy.context.object.modifiers["Mesh Cache"].forward_axis = 'NEG_Z'
                bpy.context.object.modifiers["Mesh Cache"].up_axis = 'POS_Y'
                bpy.context.object.modifiers["Mesh Cache"].flip_axis = {'X'}

                export_path = ImportAllObj.getexportfolder(self)
                d = []  # Holds all directory names
                for (dirpath, dirnames, filenames) in os.walk(export_path):
                    d.extend(dirnames)
                    break

                for folder in d:
                    if "collar_cloth" in child.name:
                        folder_path = export_path + "\\" + "LittleAllan"
                        for root, dirs, files in os.walk(folder_path):
                            for fil in files:
                                if fil.endswith(mdd_file_name):
                                    import_path = folder_path + "\\" + str(fil)
                                    print(import_path)
                                    bpy.data.objects[child.name].modifiers["Mesh Cache"].filepath = import_path
                        break
                    if folder == child.parent.name[:-5]:
                        folder_path = export_path + "\\" + folder
                        for root, dirs, files in os.walk(folder_path):
                            for fil in files:
                                if fil.endswith(mdd_file_name):
                                    import_path = folder_path + "\\" + str(fil)
                                    print (import_path)
                                    bpy.data.objects[child.name].modifiers["Mesh Cache"].filepath = import_path
                                else:
                                    continue
                    else:
                        continue
            else:
                prompt("Could not add cache - object name mismatch")
        return {'FINISHED'}


class AddClothCache(bpy.types.Operator):
    """Adds cloth cache to cloth objects"""
    bl_idname = "cloth.cache"
    bl_label = "Adds cloth cache to cloth objects"

    def execute(self, context):
        curr_file_path = bpy.data.filepath
        main_folder = os.path.dirname(curr_file_path)
        main_folder = main_folder.replace("render", "vfx")
        vfx_export_folder = main_folder+"//export/"

        d = []  # Holds all directory names
        for (dirpath, dirnames, filenames) in os.walk(vfx_export_folder):
            d.extend(dirnames)
            break

        if bpy.data.objects.get("cloth") is None:
            bpy.ops.object.empty_add(type='PLAIN_AXES', radius=1, view_align=False, location=(0, 0, 0))
            for obj in bpy.context.selected_objects:
                obj.name = "cloth"


        for folder in d:
            folder_path = vfx_export_folder + "\\" + folder    # Parent folder
            for root, dirs, files in os.walk(folder_path):      # Look in parent folder for obj files
                for fil in files:
                    if fil.endswith('.obj'):
                        import_path = folder_path + "\\" + str(fil)
                        bpy.ops.import_scene.obj(filepath=import_path, use_image_search=True, split_mode='OFF')

                        cloth_obj = bpy.context.selected_objects[0]
                        bpy.context.scene.objects.active = cloth_obj
                        bpy.context.object.scale[0] = -1 # Mesh cache mirror fix
                        bpy.ops.object.shade_smooth()

                        if 'MESH_CACHE' not in (mod.type for mod in cloth_obj.modifiers):
                            bpy.ops.object.modifier_add(type='MESH_CACHE')

                        bpy.context.object.modifiers["Mesh Cache"].forward_axis = 'POS_Y'
                        bpy.context.object.modifiers["Mesh Cache"].up_axis = 'NEG_Z'

                        import_path = import_path[:-4] + ".mdd"

                        bpy.data.objects[cloth_obj.name].modifiers["Mesh Cache"].filepath = import_path

                        a = bpy.data.objects["cloth"]
                        bpy.ops.object.select_all(action='DESELECT')  # deselect all object
                        a.select = True
                        cloth_obj.select = True  # select the object for the 'parenting'
                        bpy.context.scene.objects.active = a  # the active object will be the parent
                        bpy.ops.object.parent_set()


        return {'FINISHED'}