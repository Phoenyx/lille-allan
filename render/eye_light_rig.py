__author__ = 'Sofia'

import bpy
from utils.script_utils import prompt


class EyeLightRig(bpy.types.Operator):
    '''Creates a rig for eye lights'''
    bl_idname = "eye_light.rig"
    bl_label = "Eye Light Rig"

    def get_eye(self):
        # Select the eye mesh
        try:         # Is anything selected
            selected = bpy.context.selected_objects[0]
            if selected.type == "MESH":         # Is the selection of type mesh
                eye = bpy.context.selected_objects[0]
                bpy.context.scene.objects.active = eye
                return eye
            else:
                prompt("Please select eye mesh", icon="ERROR")
                return False
        except IndexError:
            prompt("No object selected", icon="ERROR")
            return False

    def get_empty(self):
        # Get the eye's vertex no. 0 world position
        eye = EyeLightRig.get_eye(self)
        vertex_zero = eye.data.vertices[0]

        vertex_world_pos = eye.matrix_world * vertex_zero.co

        # Create an Empty on that world position
        bpy.ops.object.empty_add(type='PLAIN_AXES', view_align=False, location=vertex_world_pos)

        # Rename the newly created Empty
        empty = bpy.context.selected_objects[0]
        empty.name = eye.name+"_LightHolder"

        return empty

    def execute(self, context):
        if EyeLightRig.get_eye(self):
            EyeLightRig.make_vertex_parent(self, EyeLightRig.get_eye(self), EyeLightRig.get_empty(self))
            prompt("Rig setup!")
            return {'FINISHED'}
        else:
            return {'FINISHED'}

    def make_vertex_parent(self, eye_mesh, the_empty):

        # Go to object mode
        bpy.ops.object.mode_set(mode='OBJECT')

        # Deselect everything
        bpy.ops.object.select_all(action='DESELECT')

        # Select the empty
        the_empty.select = True
        # Set the active object to be the eye mesh
        bpy.context.scene.objects.active = eye_mesh

        # Set the active area in blender to be the 3d view
        for area in bpy.context.screen.areas:
            if area.type == 'VIEW_3D':
                area.spaces.active.region_3d.view_matrix
                break

        # Go to edit mode to de-select all previous selected vertices
        bpy.ops.object.mode_set(mode='EDIT')
        # Choose the select mode to be vertices (edges, faces = False)
        bpy.context.tool_settings.mesh_select_mode = [True, False, False]

        # Deselect all the vertices
        bpy.ops.mesh.select_all(action='DESELECT')

        # Got to be in Object mode to select the vertices
        bpy.ops.object.mode_set(mode='OBJECT')

        # Select the first 3 vertices
        eye_mesh.data.vertices[0].select = True
        eye_mesh.data.vertices[1].select = True
        eye_mesh.data.vertices[2].select = True

        # Go back to edit mode
        bpy.ops.object.mode_set(mode='EDIT')

        # Run the vertex parent function
        bpy.ops.object.vertex_parent_set()

        # Go back to Object mode
        bpy.ops.object.mode_set(mode='OBJECT')

        # Add light and place it in front of the empty (default position)
        empty_loc = bpy.data.objects[the_empty.name].location       # get the empty location
        pos_vector = (empty_loc[0], empty_loc[1]-0.09, empty_loc[2])    # Offset the y-position a little

        bpy.ops.object.lamp_add(type='POINT', radius=1, view_align=False, location=pos_vector)
        light = bpy.context.selected_objects[0]
        light.name = "Rigged_Eye_Light"

        # Parent light under the empty
        the_empty.select = True
        bpy.context.scene.objects.active = the_empty
        bpy.ops.object.parent_set(type='OBJECT', keep_transform=False)
        bpy.ops.object.select_all(action='DESELECT')

