__author__ = 'render'
import os
import re

import bpy
from utils.script_utils import prompt
from utils.script_utils import ANIM_FILE_NAME_REGEX_NAME


class ImportCamera(bpy.types.Operator):
    '''Imports the camera from the relevant animations scene export folder'''
    bl_idname = "imp.cam"
    bl_label = "Imports camera from animation scene"

    ANIM_NAME = ANIM_FILE_NAME_REGEX_NAME

    def get_export_folder(self):
        scene_file_name_ext = os.path.splitext(bpy.path.basename(bpy.context.blend_data.filepath))
        filename = str(scene_file_name_ext[0])
        if re.match(self.ANIM_NAME, filename):
            curr_file_path = bpy.data.filepath
            main_folder = os.path.dirname(curr_file_path)
            main_folder = main_folder.replace("render", "animation")
            export_folder = main_folder + "\export\\"
            return export_folder

        else:
            prompt("Could not import camera - check scene name!")
            return {'FINISHED'}

    def execute(self, context):
        if self.get_export_folder():
            the_folder = self.get_export_folder()

            scene_file_name_ext = os.path.splitext(bpy.path.basename(bpy.context.blend_data.filepath))
            filename = str(scene_file_name_ext[0])
            filename = filename[:-8]        # Take away initials and version number "_v001_rr" = 8 chars

            cam_file_path = the_folder + filename+"_mainCamera.fbx"

            if os.path.exists(cam_file_path):
                bpy.ops.import_scene.fbx(filepath=cam_file_path, directory=self.get_export_folder(), axis_forward='Y', axis_up='Z')
                prompt("Imported camera!")
            else:
                prompt("No camera found in: "+str(self.get_export_folder()))
        else:
            prompt("Could not import camera - check scene name!")
        return {'FINISHED'}