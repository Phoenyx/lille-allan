
__author__ = 'Sofia'
import os


class LightList():
    @staticmethod
    def path(self):
        return r"D:\ProjectName\02_pro\render"


def get_light_data(self, context):
    output = {}
    path = LightList.path(self)

    # looks at the content in the render directory and adds them to the list if they are directories/folders
    sequence_folders = [f for f in os.listdir(path) if os.path.isdir(os.path.join(path, f))]

    for sequence_folder in sequence_folders:
        output[sequence_folder] = {}
        sequence_path = os.path.join(path, sequence_folder)

        # looks at the content in the sequences directories and adds them to the list if they are directories/folders
        scene_folders = [f for f in os.listdir(sequence_path) if os.path.isdir(os.path.join(sequence_path, f))]

        for scene_folder in scene_folders:
            scene_path = os.path.join(sequence_path, scene_folder)

            # looks at the content in the scene directories and adds them to the list if they are directories/folders
            shot_folders = [f for f in os.listdir(scene_path) if os.path.isdir(os.path.join(scene_path, f))]

            for shot_folder in shot_folders:
                export_path = os.path.join(scene_path, shot_folder, "export")
                try:
                    fbx_lights = [f for f in os.listdir(export_path) if ".fbx" in f] # add another check that its a light

                    if scene_folder in output[sequence_folder] is not None:
                        output[sequence_folder][scene_folder].extend(fbx_lights)
                    else:
                        output[sequence_folder][scene_folder] = fbx_lights

                except WindowsError:
                    pass

    import json
    print(json.dumps(output))

    return output


def get_filtered_data(data, sequence_filter=None, scene_filter=None):
    sequences = []
    scenes = []
    fbx_lights = []

    for sequence in data.keys():
        sequences.append((sequence, sequence, ""))

         # key     # value          # returns the keys and values
    for sequence, sequence_dict in data.items():
        if sequence_filter is None or sequence_filter in sequence:
            print sequence

            for scene, scene_dict in sequence_dict.items():
                scenes.append((scene, scene, ""))

                if scene_filter is None or scene_filter in scene:
                    print scene, scene_dict
                    for fbx_light in scene_dict:
                        print fbx_light
                        fbx_lights.append((fbx_light, fbx_light, ""))

    return sequences, scenes, fbx_lights


xyz = get_light_data(None, None)
# use indexes to get sq, sc and lights lists for blender drop down menu
print (get_filtered_data(xyz, sequence_filter="SQ0030", scene_filter="SC0010")[2])



# NEXT
import bpy

def make_vertex_parent(target_object,ob_to_distribute):

    bpy.ops.object.mode_set(mode='OBJECT')

    ob_to_distribute.select=True
    bpy.context.scene.objects.active=target_object

    target_object = bpy.context.active_object

    for area in bpy.context.screen.areas:
        if area.type == 'VIEW_3D':
            area.spaces.active.region_3d.view_matrix
            break

    bpy.ops.object.mode_set(mode='EDIT')
    bpy.context.tool_settings.mesh_select_mode=[True,False,False]
    bpy.ops.mesh.select_all(action='DESELECT')

    bpy.ops.object.mode_set(mode='OBJECT')
    target_object.data.vertices[0].select = True
    target_object.data.vertices[1].select = True
    target_object.data.vertices[2].select = True


    bpy.ops.object.mode_set(mode='EDIT')

    #now in edit mode with mushroom selected and appropriate verts selected

    bpy.ops.object.vertex_parent_set()

    #out of edit mode
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')
    print ("DONE")



R_eye = bpy.context.selected_objects[0]
from bpy import context
bpy.context.scene.objects.active=R_eye
vertex_zero = R_eye.data.vertices[0]

vertex_world_pos = R_eye.matrix_world * vertex_zero.co

bpy.ops.object.empty_add(type='PLAIN_AXES', view_align=False, location=(vertex_world_pos))

Empty = bpy.context.selected_objects[0]
Empty.name = R_eye.name+"_LightHolder"


bpy.ops.object.select_all(action='DESELECT')


make_vertex_parent(R_eye, Empty)
