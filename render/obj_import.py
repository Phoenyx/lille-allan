__author__ = 'render'
import os
import re

import bpy
from utils.script_utils import prompt
from utils.script_utils import ANIM_FILE_NAME_REGEX_NAME


class ImportAllObj(bpy.types.Operator):
    '''Imports all objects from the relevant animation/export folder'''
    bl_idname = "imp.obj"
    bl_label = "Import Selected As Obj's"
    i = 0

    ANIM_NAME = ANIM_FILE_NAME_REGEX_NAME

    def getexportfolder(self):
        scene_file_name_ext = os.path.splitext(bpy.path.basename(bpy.context.blend_data.filepath))
        filename = str(scene_file_name_ext[0])

        if re.match(self.ANIM_NAME, filename):
            curr_file_path = bpy.data.filepath
            main_folder = os.path.dirname(curr_file_path)
            main_folder = main_folder.replace("render", "animation")
            export_folder = str(main_folder + "\export\\")
            return export_folder

        else:
            prompt("Could not import obj(s) - check scene name!")
            return {'FINISHED'}

    def execute(self, context):
        if self.getexportfolder():
            d = []  # Holds all directory names
            for (dirpath, dirnames, filenames) in os.walk(self.getexportfolder()):
                d.extend(dirnames)
                break

            for name in d:
                if bpy.data.objects.get(name + "_Mesh") is None:
                    bpy.ops.object.empty_add(type='PLAIN_AXES', radius=1, view_align=False, location=(0, 0, 0))
                    for obj in bpy.context.selected_objects:
                        obj.name = name + "_Mesh"

            for folder in d:
                folder_path = self.getexportfolder() + "\\" + folder    # Parent folder
                for root, dirs, files in os.walk(folder_path):      # Look in parent folder for obj files
                    for fil in files:
                        if fil.endswith('.obj'):
                            if not fil.endswith('_cloth.obj') or fil.endswith('shorts_cloth.obj'):
                                self.i += 1                         # count how many have been imported
                                import_path = folder_path + "\\" + str(fil)
                                bpy.ops.import_scene.obj(filepath=import_path, use_image_search=True, split_mode='OFF')
                                # Parent the imported obj
                                b = bpy.context.selected_objects[0]
                                bpy.context.scene.objects.active = b
                                bpy.ops.object.shade_smooth()
                                bpy.context.object.scale[0] = -1    # Mesh cache mirror fix
                                a = bpy.data.objects[folder + "_Mesh"]
                                bpy.ops.object.select_all(action='DESELECT')  # deselect all object
                                a.select = True
                                b.select = True  # select the object for the 'parenting'
                                bpy.context.scene.objects.active = a  # the active object will be the parent
                                bpy.ops.object.parent_set()
                            else:
                                continue
                        else:
                            continue

            prompt("Imported " + str(self.i) + " objects")

        else:
            prompt("Could not import obj(s) - check scene name!")
        return {'FINISHED'}