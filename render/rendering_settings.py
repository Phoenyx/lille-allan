__author__ = 'render'
import bpy


class SetRenderingSettings(bpy.types.Operator):
    bl_idname = "rendering.settings"
    bl_label = "Sets the general render settings"

    def execute(self, context):
        bpy.ops.object.select_all(action='DESELECT')
        bpy.context.scene.world.cycles.sample_as_light = True
        bpy.context.scene.world.cycles.sample_map_resolution = 1024
        bpy.context.scene.cycles.blur_glossy = 0.5
        bpy.context.scene.cycles.caustics_reflective = False
        bpy.context.scene.cycles.caustics_refractive = False
        bpy.context.scene.cycles.sample_clamp_indirect = 3
        bpy.context.scene.cycles.samples = 500
        bpy.context.scene.cycles.film_transparent = True
        bpy.context.scene.render.display_mode = 'WINDOW'
        bpy.context.scene.cycles.aa_samples = 300
        bpy.context.scene.cycles.preview_aa_samples = 2
        bpy.context.scene.cycles.diffuse_samples = 3
        bpy.context.scene.cycles.subsurface_samples = 2
        bpy.context.scene.cycles.sample_clamp_indirect = 5
        bpy.context.scene.cycles.sample_clamp_direct = 5
        bpy.context.scene.cycles.volume_max_steps = 512
        bpy.context.scene.cycles.volume_step_size = 3
        bpy.context.scene.cycles.transparent_max_bounces = 2
        bpy.context.scene.cycles.transparent_min_bounces = 2
        bpy.context.scene.cycles.max_bounces = 2
        bpy.context.scene.cycles.min_bounces = 2
        bpy.context.scene.cycles.diffuse_bounces = 2
        bpy.context.scene.cycles.glossy_bounces = 1
        bpy.context.scene.cycles.transmission_bounces = 0
        bpy.context.scene.cycles.blur_glossy = 5
        bpy.context.scene.cycles.transmission_bounces = 1
        bpy.context.scene.render.tile_x = 256
        bpy.context.scene.render.tile_y = 256
        bpy.context.scene.world.cycles.sample_map_resolution = 256
        bpy.context.scene.world.cycles.samples = 1


        return {'FINISHED'}



"""
Side notes:

    Glossy shader: value 0.5, roughness: 0.025. Use point light (size: 3mm) to create highlight.
    AO shader: plain/standard diffuse shader.

"""



class MakePathsRelative(bpy.types.Operator):
    bl_idname = "rel.paths"
    bl_label = "Makes all paths connected to the scene relative"

    def execute(self, context):
        bpy.ops.file.make_paths_relative()
        return {'FINISHED'}
