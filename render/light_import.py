__author__ = 'Sofia'
import bpy
import os
from bpy.app.handlers import persistent
from utils.script_utils import prompt
from utils.light_settings_reader import get_setting

class ImportLightsFromList(bpy.types.Operator):
    bl_label = "import the lights"
    bl_idname = "get.lights"

    def execute(self, context):

        lights_to_import = bpy.data.window_managers["WinMan"].light_files
                                # render        # SQ            # SC            # SH            # .blend file
        render_folder_path = (os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(bpy.data.filepath)))))

        # split string at the underscores
        SQ, SC, SH, l = lights_to_import.split('_')

        lights_dir_path = str(render_folder_path) + r"\\" + SQ + r"\\" + SC + r"\\" + SH + r"\\" + r"export/"

        lights_file_path = str(render_folder_path) + r"\\" + SQ + r"\\" + SC + r"\\" + SH + r"\\" + r"export/" + lights_to_import

        lights_txt_file_path = lights_file_path[:-3] + "txt"

        if os.path.exists(lights_file_path):
            bpy.ops.import_scene.fbx(filepath=lights_file_path, directory=lights_dir_path, axis_forward='Z', axis_up='Y')

            # add all the newly imported lights to a list
            lights = bpy.context.selected_objects

            # activate "use nodes" for all lights
            for light in lights:
                bpy.context.scene.objects.active = light
                light.data.use_nodes = True

            errors = []

            for light in lights:
                matched = False
                bpy.context.scene.objects.active = light
                for i in range(1, len(bpy.context.selected_objects)+1):
                    # Use the get_setting function to get the light settings
                    if get_setting("name", "Light" + str(i), lights_txt_file_path) == light.name:

                        matched = True

                        # Set all the settings from the .txt file
                        if str(get_setting("cast shadows", "Light" + str(i), lights_txt_file_path)) == "True":
                            light.data.cycles.cast_shadow = True
                        else:
                            light.data.cycles.cast_shadow = False

                        light.data.shadow_soft_size = float(get_setting("size", "Light" + str(i), lights_txt_file_path))

                        light.data.cycles['max_bounces'] = int(get_setting("max bounces", "Light" + str(i), lights_txt_file_path))

                        data = light.data
                        data.node_tree.nodes["Emission"].inputs[1].default_value = float(get_setting("strength", "Light" + str(i), lights_txt_file_path))

                        color = get_setting("color", "Light" + str(i), lights_txt_file_path)

                        r, g, b = color.split(',')

                        data.node_tree.nodes["Emission"].inputs[0].default_value[0] = float(r)
                        data.node_tree.nodes["Emission"].inputs[0].default_value[1] = float(g)
                        data.node_tree.nodes["Emission"].inputs[0].default_value[2] = float(b)

                        if light.type == 'SPOT':
                            light.data.spot_size = int(get_setting("cone size", "Light" + str(i), lights_txt_file_path))
                            light.data.spot_blend = float(get_setting("cone blend", "Light" + str(i), lights_txt_file_path))

                    else:
                        continue

                if not matched:
                    # Add light to the list of errors if its name didn't match any name in the .txt file
                    errors.append(light)

            num_lights = len(lights)
            num_errors = len(errors)

            if num_errors > 0:
                prompt("Lights imported: " + str(num_lights) + " with "
                       + str(num_errors) + " errors. Look in console for more.", icon="ERROR")

                print ("Following lights did not get settings set:")
                for error in errors:
                    print (error.name)
                print ("To fix: Undo import and rename existing lights/objects")

            else:
                prompt("Lights imported: " + str(num_lights) + " with 0 errors")

        else:
            prompt("No lights found in: "+lights_dir_path)

        return {'FINISHED'}
