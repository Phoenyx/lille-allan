__author__ = 'render'
import os
import re

import bpy
from utils.script_utils import prompt
from utils.script_utils import ANIM_FILE_NAME_REGEX_NAME


class ExportLightsBlender(bpy.types.Operator):
    '''Exports all the light objects in the scene'''
    bl_idname = "blender.lights"
    bl_label = "Export all the light objects in the scene"

    ANIM_NAME = ANIM_FILE_NAME_REGEX_NAME

    def execute(self, context):
        # Get the .blend scenes name
        scene_file_name_ext = os.path.splitext(bpy.path.basename(bpy.context.blend_data.filepath))
        filename = str(scene_file_name_ext[0])

        # Find the blender files export folder
        curr_file_path = bpy.data.filepath
        main_folder = os.path.dirname(curr_file_path)
        export_folder = main_folder + "\export\\"

        if re.match(self.ANIM_NAME, filename):      # Check if the scene we are in is named correctly
            matches = re.search(self.ANIM_NAME, filename)
            groups = matches.groups()
            filename = groups[0]
        else:
            prompt("Failed to export - check scene naming.")
            return {'FINISHED'}

        if not os.path.exists(export_folder):       # Make export folder if it doesnt exist.
            print("Creating {0}".format(export_folder))
            os.makedirs(export_folder)

        i = 0       # Is the Light number we are at
        write_string = ''

        for obj in bpy.data.objects:
            obj_type = obj.type
            if obj_type == 'LAMP':      # Only continue if the object is a light
                data = obj.data
                obj.select = True
                i += 1
                name = obj.name

                light_type = obj.data.type

                shadows = str(obj.data.cycles.cast_shadow)      # Shadows: True/False

                size = str(float(round(obj.data.shadow_soft_size, 3)))  # Attenuation

                max_bounces = str(int(obj.data.cycles['max_bounces']))

                strength = str(data.node_tree.nodes["Emission"].inputs[1].default_value)  # Intensity

                color = (str(data.node_tree.nodes["Emission"].inputs[0].default_value[0]) + ',' +
                         str(data.node_tree.nodes["Emission"].inputs[0].default_value[1]) + ',' +
                         str(data.node_tree.nodes["Emission"].inputs[0].default_value[2]))

                cone_size = "None"          # Points lights do not have cone sizes or cone blends
                cone_blend = "None"

                if light_type == 'SPOT':
                    cone_size = str(round(obj.data.spot_size * 57.2957795, 2))
                    cone_blend = str(float(round(obj.data.spot_blend, 3)))

                # Write all settings to string

                write_string += "[Light" + str(i) + "]" + '\r\n' \
                                "name = " + name + '\r\n' \
                                "type = " + light_type + '\r\n' \
                                "cast shadows = " + shadows + '\r\n' \
                                "size = " + size + '\r\n' \
                                "max bounces = " + max_bounces + '\r\n' \
                                "strength = " + strength + '\r\n' \
                                "color = " + color + '\r\n' \
                                "cone size = " + cone_size + '\r\n' \
                                "cone blend = " + cone_blend + '\r\n'

        export_path_string = export_folder + filename + "_allLights.txt"        # Get export path for .txt
        export_path = export_folder + filename + "_allLights.fbx"               # Get export path for .fbx

        if len(bpy.context.selected_objects) >= 1:                              # If 1 or more objects were selected

            f = open(export_path_string, 'w')                   # Create the .txt doc and write the settings
            f.write(write_string)
            f.close()

            # Export the lights
            bpy.ops.export_scene.fbx(filepath=export_path, filter_glob="*.fbx", axis_forward='Y', axis_up='Z',
                                     bake_anim_use_nla_strips=False, bake_anim_use_all_actions=False,
                                     use_selection=True)

            prompt("all lights exported to " + export_path)     # FEEDBACK TO USER

        else:
            prompt("No lights found")  # FEEDBACK TO USER

        return {'FINISHED'}