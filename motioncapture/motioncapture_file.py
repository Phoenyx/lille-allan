__author__ = 'render'
import os

import bpy
from motioncapture.fix_framerate import fix_mocap_framerate
from utils.script_utils import prompt


class MakeAnimReadyMotionCaptureFile(bpy.types.Operator):
    """Creates a Panel in the Object properties window"""
    bl_label = "Mocap Tools"
    bl_idname = "make.ready"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = "LA Tools"

    this_path = r"M:\projects\Lille Allan\TeaserProduction\02_production\mocap"

    def find_path(self, name, path):
        for root, dirs, files in os.walk(path):
            if name in files:
                return os.path.join(root, name)

    def set_path(self, name, path):
        for root, dirs, files in os.walk(path):
            if name in files:
                return os.path.join(root, "scaled_" + name)

    def execute(self, context):
        bvh_file_to_import = bpy.data.window_managers["WinMan"].bvh_files
        file_path = self.find_path(bvh_file_to_import, self.this_path)

        if bpy.ops.import_anim.bvh(filepath=file_path, rotate_mode='NATIVE', axis_forward='-Z', axis_up='Y'):
            print("importing")

        fix_mocap_framerate()

        new_file_path = self.set_path(bvh_file_to_import, self.this_path)
        if bpy.ops.export_anim.bvh(filepath=new_file_path, rotate_mode='NATIVE',
                                   frame_end=bpy.context.scene.frame_end):
            print("exporting")

        prompt("DONE!")
        return {'FINISHED'}