__author__ = 'render'
import bpy
import math


class FixMocapFramerate(bpy.types.Operator):
    bl_idname="fix.motioncapture"
    bl_label="hello"

    def execute(self, context):
        fix_mocap_framerate()
        return {'FINISHED'}


def fix_mocap_framerate():

    end_frame = 0

    for action in bpy.data.actions:
        for fcurve in action.fcurves:
            for point in fcurve.keyframe_points:
                if point.co.x > end_frame:
                    end_frame = point.co.x      # Find highest x value on curves

    end_frame = (end_frame/2.4)      # New frame count after frame rate convertion
    
    end_frame = round(end_frame, 0)     # Get whole number
    
    bpy.context.scene.frame_end = end_frame     # Set end frame for exporting later

    bpy.data.scenes["Scene"].render.frame_map_old = 100
    bpy.data.scenes["Scene"].render.frame_map_new = 42      # 100/2.4 = 42
    bpy.ops.script.python_file_run(filepath="C:\\Program Files\\Blender Foundation\\Blender\\2.73\\scripts\\presets\\framerate\\25.py")


